"""A module for the calculations of an rhs welded T-joint"""

import PySS.steel_design as sd


def joint(side_1, side_2, thick, f_y):

    # Keep side_1 as the largest side
    if side_2 > side_1:
        side_1, side_2 = side_2, side_1

    shs = sd.CsProps.from_rhs(side_1, side_2, thick)
    flat_1 = side_1 - 6*thick
    flat_2 = side_1 - 6*thick

    p1_class = sd.plate_class_new(thick, flat_1, f_y)
    p2_class = sd.plate_class_new(thick, flat_2, f_y)

    sln_1 = flat_1 / (thick * (235. / f_y)**0.5)
    sln_2 = flat_2 / (thick * (235. / f_y)**0.5)

    print(p1_class, p2_class)
    print(sln_1, sln_2)

def main():
    # joint(100, 100, 3, 355)
    # joint(120, 120, 4, 355)
    joint(160, 160, 5, 700)

if __name__ == "__main__":
    main()
