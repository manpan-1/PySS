"""
Module for describing and processing bolt-nut assemblies.

Contains classes and functions that were created while researching the behaviour of a structural fastener assembly. The
work includes methods for importing, modifying, plotting etc. experimental tensile test data, tailored to be used with
the experimental data from the relevant reseach project in simlab, NTNU.
This project was initiated by Erik Grimsmo [1] with series of tests on bolt assemblies and FEM modedlling in Abaqus. The
present script is created afterwards, as an attempt to re-evaluate and extend those experiments and conclusions.

[1] E. L. Grimsmo, A. Aalberg, M. Langseth, and A. H. Clausen, "Failure modes of bolt and nut assemblies under
    tensile loading" Journal of Constructional Steel Research, vol. 126, pp. 15-25, Nov. 2016.

"""

import pickle
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os

# Eneble TeX rendering for matplotlib
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams.update({'font.size': 14})


class Experiment:
    """Class of a typical laboratory experiment"""
    def __init__(
            self,
            name=None
    ):
        if name is None:
            name = "new_test"

        self.name = name
        self.test_data = None

    def __repr__(self):
        return "<class " + self.__class__.__name__ + ": " + self.name + ">"

    @classmethod
    def from_load_experiment(
            cls,
            filename,
            name=None
    ):
        """"""
        if name is None:
            name = filename

        bolt = cls(name=name)
        bolt.load_lab_csv(filename)

        return bolt

    @classmethod
    def from_load_abq_pickle(
            cls,
            filename,
            name=None
    ):
        """"""
        if name is None:
            name = filename

        experiment = cls(name=name)
        experiment.load_abq_pickle(filename)

        return experiment

    def load_lab_csv(self, filename):
        """Read a .csv file with experimental data"""

        self.test_data = pd.read_csv(
            filename,
            delimiter=";",
            decimal=",",
            skiprows=[0, 1, 2, 4]
        )

    def load_abq_pickle(self, filename):
        """Read a .pkl file with data from a relevant abaqus export"""

        with open(filename, "rb") as fh:
            results = pickle.load(fh)

        self.test_data = pd.DataFrame.from_dict(results)

    def smooth(self, channel, box_pts):
        """
        Smooth the data of a channel.

        Smooth data method based on a moving average box by convolution.

        Parameters
        ----------
        channel : str
            Name of the channel to be smoothed
        box_pts : int
            width of the smoothing box.

        """
        box = np.ones(box_pts) / box_pts
        self.test_data[channel] = np.convolve(self.test_data[channel], box, mode='same')

    def plot2d(
            self,
            xdata,
            ydata,
            ax=None,
            tick_scales=None,
            **kargs
    ):
        """
        Plot two channels against each other.

        Parameters
        ----------
        xdata, ydata : str
            Names of the channels to  be plotted.
        ax : axis object, optional
            Axis to plot on. By default, a new figure and axis are created.

        """
        if ax is None:
            fig, ax = plt.subplots()

        if tick_scales is None:
            tick_scales = (1, 1)

        ax = self.test_data.plot(
            x=xdata,
            y=ydata,
            ax=ax,
            **kargs,
            legend=False
        )

        x_ticks, y_ticks = (ax.get_xticks()*tick_scales[0], ax.get_yticks()*tick_scales[1])

        ax.set_xticklabels(x_ticks)
        ax.set_yticklabels(y_ticks)

        return ax


class TensileTest(Experiment):
    """An experiment class inheriting pandas dataframe"""

    def __init__(self, name=None):
        if name is None:
            name = "new_bolt_test"

        self.elasticity = None
        self.bias = None
        super().__init__(name=name)

    def __repr__(self):
        return "<class " + self.__class__.__name__ + ": " + self.name + ">"

    def monotonise_displacement(self):
        """
        Linearise the displacement channel.

        Valid only for displacement control experiments, for which the displacements can be assumed to change in a
        fairly linear manner in time. This step is required to derive the displacements as a monotonic function of time
        which is necessary in order to obtain the reaction (load) as a function of the displacement, F(d).
        In case the displacements are not monotonic in respect to the timestamp, the load-displacement relation will
        fluctuate back and forth, thus the load can not be drawn as a function of the displacement, making it impossible
        to perform pure functional operations (most importantly, differentiation, which concludes the stiffness).

        """

        x = np.array(self.test_data.Time)
        y = np.array(self.test_data.Extension)
        stroke_speed = np.linalg.lstsq(x.reshape(-1, 1), y)[0][0]
        self.test_data.Extension = self.test_data.Time * stroke_speed

    def get_elastic_stiffness(self):
        """Calculate the elastic stiffness of the tensile test"""

        highpass = 0.2 * self.test_data["Load"].max()
        lowpass = 0.6 * self.test_data["Load"].max()

        min_idx = self.test_data[self.test_data["Load"] > highpass].index[0]
        max_idx = self.test_data[self.test_data["Load"] > lowpass].index[0]

        el_regime = self.test_data.iloc[min_idx:max_idx]

        linear = np.polyfit(el_regime["Extension"], el_regime["Load"], 1)

        k_11 = linear[0]
        bias = linear[1]
        self.elasticity = k_11
        self.bias = bias

    def adjust_init_stiffness(self, tol=None):
        """
        Adjust the initial stiffness to remove the effect of fittings.

        Initial data are removed until they converge to the calculated elasticity up to a given tolerance. The
        elasticity of the test must be calculated first.

        Parameters
        ----------
        tol : float
            Initial data removed until they meet this tolerance

        """
        #TODO: express this tolerance as a fraction og the elasticity. This absolute definition can cause troubles
        if tol is None:
            tol = 100

        init_idx = self.test_data[
            np.abs(self.test_data["Load"]-(self.test_data["Extension"]*self.elasticity+self.bias)) < tol
        ].index[0]
        self.test_data.drop(index=np.r_[0:init_idx], inplace=True)
        self.test_data.reset_index(drop=True, inplace=True)

        self.test_data["Extension"] = self.test_data["Extension"] + self.bias/self.elasticity
        self.bias = 0

        init_zero = pd.DataFrame({"Time": [0], "Load": [0], "Extension": [0], })

        self.test_data = pd.concat([init_zero, self.test_data], ignore_index=True, sort=False)

    def remove_init_zero_load(self):
        """
        Applicable only on fem data, where the initial reaction may be zero during several initial increments.

        """

        # Detect initial zero extension values (lagging possibly due to inertial effects)
        row = 0
        for row in range(len(self.test_data["Load"])):
            if not self.test_data["Load"].iloc[row] == 0:
                break

        self.test_data.drop(range(1, row), inplace=True)
        self.test_data.reset_index(drop=True, inplace=True)

    def calc_plastic_extension(self):
        self.test_data["Plastic_extension"] = self.test_data["Extension"] - self.test_data["Load"] / self.elasticity

    def calc_stiffness(self):
        """Calculate the stiffness by differentiating the extension-load data"""
        # print(len(np.diff(self.test_data.Extension)), len(np.diff(self.test_data.Load)))
        rate = np.divide(np.diff(self.test_data.Load), np.diff(self.test_data.Extension))
        self.test_data["Stiffness"] = np.concatenate(([rate[0]], rate))

        # print(len(self.test_data.Load), len(np.diff(self.test_data.Load)))

    def plot_tensile_test(
            self,
            ax=None,
            plot_stiffness=None,
            plot_plastic=None,
            **kargs
    ):
        """
        Plot load-displacement, plastic extension and stiiffenss curves.

        Parameters
        ----------
        ax :
        plot_stiffness :
        plot_plastic :

        """

        if plot_stiffness is None:
            plot_stiffness = False

        if plot_plastic is None:
            plot_plastic = False

        ax = self.plot2d("Extension", "Load", ax=ax, **kargs)

        if plot_stiffness:
            self.plot2d("Extension", "Stiffness", ax=ax)

        if plot_plastic:
            self.plot2d("Plastic_extension", "Load", ax=ax)

        return ax


class BoltAssembly:
    """A bolt assembly class"""

    def __init__(
            self,
            bolt_size=None,
            grip_length=None,
            threaded_grip_length=None,
            bolt_grade=None,
            name=None,
            number=None,
            thread_type=None,
    ):
        if name is None:
            self.name = "_".join(
                [
                    str(bolt_size),
                    str(bolt_grade),
                    str(thread_type),
                    str(grip_length),
                    str(threaded_grip_length)
                ]
            )
        else:
            self.name = name

        self.number = number
        self.bolt_size = bolt_size
        self.bolt_grade = bolt_grade
        self.thread_type = thread_type
        self.grip_length = grip_length
        self.threaded_grip_length = threaded_grip_length

        self.connector = None
        self.experiment = None

    def __repr__(self):
        return "<class " + self.__class__.__name__ + ": " + self.name + ", M{} {}, l_g = {}, l_t = {}".format(
            self.bolt_size,
            self.bolt_grade,
            self.grip_length,
            self.threaded_grip_length
        ) + ">"

    @classmethod
    def from_load_connector(cls, filename):
        """"""
        bolt = cls(name=filename)
        bolt.load_connector(filename)

        return bolt

    @classmethod
    def from_load_experiment(
            cls,
            filename,
            name=None,
            **kargs
    ):
        """"""
        if name is None:
            name = "new_bolt_assembly"
        bolt = cls(name=name)
        bolt.load_experiment(
            filename,
            **kargs
        )

        return bolt

    def theoretical_shank_elastic_stiffness(self):
        """The theoretical elastic stiffness of the bolt shank for the nominal dimensions."""

        young = 210000
        poisson = 0.3

        radius = self.bolt_size / 2
        area = np.pi * radius ** 2
        moi = np.pi * radius ** 4 / 4
        mot = np.pi * radius ** 4 / 2
        shear_modulus = young / (2 * (1 + poisson))

        # The following stiffness matrix is derived assuming the bolt as a typical Bernoulli beam.

        # Translational
        # Axial elastic stiffness
        # k11 = area * young / self.grip_length
        k11 = 0.784*young*area / (self.threaded_grip_length + 0.784*(self.grip_length - self.threaded_grip_length))

        # Shear elastic stiffness
        k22 = 12 * young * moi / self.grip_length ** 3
        k33 = k22

        # Angular
        # Torsional elastic stiffness
        k44 = shear_modulus * mot / self.grip_length

        # Rotational elastic stiffness
        k55 = 4 * young * moi / self.grip_length
        k66 = k55

        # Assemble the stiffness vector
        k_vector = (k11, k22, k33, k44, k55, k66)

        return k_vector

    def load_connector(self, filename):
        """Read a pickle with the Load-Displacement data"""

        with open(filename, "rb") as fh:
            res = np.array(pickle.load(fh))

        self.connector = res

    def load_experiment(
            self,
            filename,
            isfem=None
    ):
        """Read a .csv file with experimental data"""

        if isfem is None:
            isfem = False

        if isfem:
            self.experiment = TensileTest.from_load_abq_pickle(filename)
        else:
            self.experiment = TensileTest.from_load_experiment(filename)

    def plot_connector(
            self,
            ax=None,
            **kargs
    ):
        """Plot results of a connector model"""

        if self.connector is None:
            print("No fem data")
            return

        if ax is None:
            fig, ax = plt.subplots()

        ax.plot(self.connector[1], self.connector[2])

        plt.grid()
        plt.show()

        return ax

    def plot_experiment(
            self,
            ax=None,
            **kargs
    ):
        """Plot results of a connector model"""

        if self.experiment is None:
            print("No experimental data")
            return

        ax = self.experiment.plot_tensile_test(ax=ax, **kargs)

        return ax


def load_bolt_experiments():
    """The main function if configured to laod Erik's data structure"""

    # bolts = pd.DataFrame([], columns=["thread_type", "l_g", "bolt_assembly"])
    bolts = []

    # Set the root directory of the data
    bolt_directory = "/home/fsol/pprojects/PySS/data/bolt_assemblies"

    # Loop through the subdirectories
    for i in os.listdir(bolt_directory):
        if len(i.split(sep="-")) == 3:
            thread_type, l_g, num = i.split(sep="-")
            l_g = float(l_g)
            num = int(num)
            if thread_type == "DG":
                l_t = l_g - 113
            else:
                l_t = l_g - 3

            # Loop through the files of each sub directory
            for j in os.listdir(bolt_directory+"/"+i):

                # If it is a csv file, read it
                if j.split(".")[-1] == "csv":

                    # Create a bolt object from the data of the csv
                    # dg_bolts[i] = BoltAssembly.from_load_experiment("/".join([bolt_directory, i, j]), name=i)
                    bolt = BoltAssembly(
                        name=i,
                        number=num,
                        thread_type=thread_type,
                        grip_length=l_g,
                        threaded_grip_length=l_t,
                        bolt_size=16,
                        bolt_grade=8.8
                    )
                    bolt.load_experiment("/".join([bolt_directory, i, j]))
                    # bolt.experiment.linearise_displacement()
                    # bolt.experiment.smooth("Load", 10)
                    # bolt.experiment.get_elastic_stiffness()
                    # bolt.experiment.adjust_init_stiffness()
                    # bolt.experiment.calc_stiffness()
                    # bolt.experiment.calc_plastic_extension()

                    bolts.append(bolt)

        # for bolt in bolts.bolt_assembly:
        #     bolt.plot_experiment()
        #     plt.show()

        # bolts.set_index(["thread_type", "l_g"], inplace=True)
    bolts_idx = pd.MultiIndex.from_tuples(
        [(x.thread_type, x.grip_length, x.number) for x in bolts],
        names=['thread_type', 'grip_length', 'number']
    )
    bolts_sr = pd.Series(bolts, index=bolts_idx)

    return bolts_sr


def plot_all_bolts():
    bolts = load_bolt_experiments()
    for idx, data in bolts.groupby(level=[0, 1]):
        fig, ax_curr = plt.subplots()
        fig.suptitle(str(idx))
        for j in data:
            j.plot_experiment(ax=ax_curr)

    plt.show()


def boxplot_nut_head_stifness(ax=None):
    if ax is None:
        fig, ax = plt.subplots()

    bolts = load_bolts()
    grouped = []
    labels = []
    for idx, data in bolts.groupby(level=[0, 1]):
        grouped.append(
            [1 / (1 / i.experiment.elasticity - 1 / i.theoretical_shank_elastic_stiffness()[0]) for i in data]
        )
        labels.append(data.index[0][0] + str(data.index[0][1]))

    ax.boxplot(grouped)
    ax.set_xticklabels(labels)
    ax.set_ylabel("K_e")

    plt.show()
    return ax


def boxplot_exp_el_stiffness(ax=None):
    if ax is None:
        fig, ax = plt.subplots()

    bolts = load_bolts()
    grouped = []
    labels = []
    for idx, data in bolts.groupby(level=[0, 1]):
        grouped.append(
            [i.experiment.elasticity for i in data]
        )
        labels.append(data.index[0][0] + str(data.index[0][1]))

    ax.boxplot(grouped)
    ax.set_xticklabels(labels)
    ax.set_ylabel("K_e")

    plt.show()
    return ax


def make_BD_28_10():

    bolt = BoltAssembly(
        bolt_size=16,
        grip_length=28,
        threaded_grip_length=10,
        bolt_grade=8.8,
        name="BD_28_10"
    )

    bolt.load_experiment("/home/fsol/pprojects/PySS/data/B_28_10_results.pkl", isfem=True)
    bolt.experiment.remove_init_zero_load()
    bolt.experiment.get_elastic_stiffness()
    bolt.experiment.adjust_init_stiffness(tol=1000)
    # bolt.experiment.smooth("Load", smoothing)
    bolt.experiment.calc_stiffness()
    bolt.experiment.calc_plastic_extension()

    bolt.load_connector("/home/fsol/pprojects/Pabq/default_wd/connector_results.pkl")

    return bolt


def main():
    theoretical = BoltAssembly(bolt_size=16, grip_length=28, threaded_grip_length=8, bolt_grade=8.8)
    fem = BoltAssembly.from_load_connector("/home/fsol/pprojects/Pabq/default_wd/connector_results.pkl")
    print(theoretical.theoretical_shank_elastic_stiffness())


if __name__ == "__main__":
    # plot_all_bolts()
    main()
