import PySS.steel_design as sd
import PySS.design_of_joints as dj
import PySS.lab_tests as lt
import logging
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

logging.basicConfig(
    level=logging.DEBUG,
    filename='logging.log',
    filemode='w',
    #format='%(name)s - %(levelname)s - %(message)s'
)

# Configure pyplot
matplotlib.rcParams.update({'font.size': 13})
matplotlib.rcParams.update({'axes.labelsize': 18})

# Add support for tex in plots.
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{xfrac}')

class TestData(lt.Experiment):
    """
    Laboratory tests of T-joint specimens

    """
    def __init__(
        self,
        name=None,
        channels=None,
        dead_weight=None,
        theoretical_specimen=None
    ):
        super().__init__(name=name, channels=channels)

        # Variable for the dead weight applied on the beam
        self.dead_weight = dead_weight

        # An object with all Eurocode/resistance calculations
        self.props = theoretical_specimen

    @classmethod
    def from_1st_batch(cls, tst_name):
        """
        Load the catman data from one of the two tests from the 1st batch.
        """
        # Create an RHS T-joint object with all the theoretical calculations
        # (properties, resistances etc.)
        sp_th = globals()[tst_name](use_nominal=False)
        sp = cls.from_catman_file(
            "./data/t_joint_data/"+tst_name+"/catman/"+tst_name+".txt"
        )

        for i in ("B1", "B2", "C1", "C2", "C3", "C4"):
            sp.channels[i]["data"] = sp.channels[i]["data"]/1e6
            sp.channels[i]["units"] = "m/m"

        del(sp.channels["Time2"])

        # Offset the Time channel so that it starts from 0
        sp.offset_channel_values(
            "Time",
            -sp.channels['Time']['data'][0]
        )

        # Downsample so that it matches the DIC sampling rate. The value 1600 is
        # used because, after trimming the tail of the experiment, we were left
        # with 1600 samples (pictures).  TODO: This corresponds only to the
        # second test. The DIC from the first test failed.
        sp.downsample_all(1600)

        # Post processing of strain gauge readings
        # ----------------------------------------
        # Offset strain-gauge channels so that they start from 0. The tare value is
        # taken as the mean value of the first 20 readings.
        for sg in ("B1", "B2", "C1", "C2", "C3", "C4"):
            sp.offset_channel_values(sg, 0-sp.channels[sg]["data"][:20].mean())

        # Calculate the bending moment of the column at the locations of the strain
        # gauges. The following calculation is valid only as long as the profile
        # remains elastic.
        sp.add_new_channel_zeros("M_u,Ed", "Nm", "M")
        sp.channels["M_u,Ed"]["data"] = 210e6*sp_th.cs_props["column"].moi_1*(
            sp.channels["C2"]["data"] - sp.channels["C1"]["data"]
        )/sp_th.geometry["height_c"]

        sp.add_new_channel_zeros("M_d,Ed", "Nm", "M")
        sp.channels["M_d,Ed"]["data"] = 210e6*sp_th.cs_props["column"].moi_1*(
            sp.channels["C4"]["data"] - sp.channels["C3"]["data"]
        )/sp_th.geometry["height_c"]

        sp.add_new_channel_zeros("M_b,Ed", "Nm", "M")
        sp.channels["M_b,Ed"]["data"] = 210e6*sp_th.cs_props["beam"].moi_1*(
            sp.channels["B2"]["data"] - sp.channels["B1"]["data"]
        )/sp_th.geometry["height_b"]

        # After measuring manually the added moment on the column due to the dead
        # load at the beam, the distribution ratio between the upper and lower part
        # of the column is calculated, normalised to the bending moment at the
        # beam.
        mm_u_Ed = (659e6)/(-3265e6)
        mm_d_Ed = (-458e6)/(-3265e6)

        # Then, the moment in the column, at the two measured locations is adjusted
        # to exclude the bending from the dead load.
        sp.add_new_channel_zeros("M_N,u,Ed", "Nm", "M")
        sp.channels["M_N,u,Ed"]["data"] = sp.channels["M_u,Ed"]["data"] - (
            mm_u_Ed*sp.channels["M_b,Ed"]["data"]
        )

        sp.add_new_channel_zeros("M_N,d,Ed", "Nm", "M")
        sp.channels["M_N,d,Ed"]["data"] = sp.channels["M_d,Ed"]["data"] - (
            mm_d_Ed*sp.channels["M_b,Ed"]["data"]
        )

        # Finally, the moment at the position of the hinges is calculated by
        # extrapolating from the two known points, up and down.
        sp.add_new_channel_zeros("M_A,Ed", "Nm", "M")
        sp.channels["M_A,Ed"]["data"] = 157*(
            sp.channels["M_N,u,Ed"]["data"] - sp.channels["M_N,d,Ed"]["data"]
        )/800 + sp.channels["M_N,u,Ed"]["data"]

        sp.add_new_channel_zeros("M_B,Ed", "Nm", "M")
        sp.channels["M_B,Ed"]["data"] = -145*(
            sp.channels["M_N,u,Ed"]["data"] - sp.channels["M_N,d,Ed"]["data"]
        )/800 + sp.channels["M_N,d,Ed"]["data"]

        # Calculate the moment in the beam by the readings of the pair of strain
        # gauges.
        sp.add_new_channel_zeros('M_ip,1', 'kNm', 'M_{ip,1,Ed}')
        sp.channels["M_ip,1"]["data"] = 210e3*sp_th.cs_props["beam"].moi_1*(
            sp.channels["B1"]["data"] - sp.channels["B2"]["data"]
        )/(sp_th.geometry["height_b"]*1e6)

        # Store the dead weight.
        sp.dead_weight = sp_th.bc_loads["dead_weight[kg]"]

        # Store entire theoretical object with the calculations
        sp.props = sp_th

        # Calculate non-dimensional loading.
        sp.add_new_channel_zeros('n,exp', '-', '$n_{chord}$')
        sp.channels["n,exp"]["data"] = sp.channels["Load"]["data"]/(
            sp_th.struct_props['n_b_rd']/1000
        )

        return(sp)

    @classmethod
    def from_2nd_batch(cls, tst_name):
        """
        Loads a test from the second batch of specimens.

        Parameters
        ----------
        tst_name : str
            Name of the requested test to be loaded. Could be  one of {"C1B10M90",
            "C1B09M70", "C2B10M70", "C2B10M90", "C2B09M70", "C2B09M90"}.

        Return
        ------
        :obj:`TestData`
            Date recorded during the test
        :obj:`PySS.design_of_joints.TjointRHS`
            Theoretical calculations for the specimen

        """
        # Start by getting all the equivalent theoretical calculations
        # for the requested experiment through the equivalent function.
        sp_th = globals()[tst_name](use_nominal=False)

        # DIC data
        # --------
        # I start with the DIC because it has lower sampling rate. The catman
        # recordings are gonna be downsampled to match the DIC.

        # Load the file.
        dic = cls.from_file(
            './data/t_joint_data/'+tst_name+'/dic.csv'
        )

        # Shorter variable for the channels
        dchn = dic.channels

        # Load the file with the initial locations (First and second row are u and
        # v respectively for camera 1, third and 4th the same for camera 2)
        with open(
            './data/t_joint_data/'+tst_name+'/init_target_loc.csv',
            'r'
        ) as fh:
            init_loc = list(lt.csv.reader(fh, delimiter=';'))

        # Offset the Time channel so that it starts from 0
        dic.offset_channel_values(
            "WindowsTime",
            -dic.channels['WindowsTime']['data'][0]
        )

        # Remove the unloading part by deleting all the data after the min
        # displacement point.
        final_pos = dchn['Displacement']['data'].argmin()
        for i in dchn.values():
            i['data'] = i['data'][:final_pos]

        # Delete the load and the displacement channels. They will be taken from
        # the catman recording instead.
        del(
            #dchn['Force'],
            dchn['Displacement']
        )

        # Invert all the vertical readings from the 2D DIC measurements to make
        # upwards positive so that it matches the abaqus model positive direction.
        for i in dchn:
            if i[0] == "v":
                dic.invert_sign(i)

        ## Calculate the shortening of the specimen between the 2 extreme points on
        ## the end plates, 0 and 19
        #dic.add_new_channel_zeros(
        #    'dv,0-19',
        #    'mm',
        #    '$\Delta v$'
        #)
        #dchn['dv,0-19']['data'] = (
        #    dchn['v_0']['data'] - dchn['v_19']['data']
        #)

        # CATMAN data
        # -----------
        # Read the ascii file
        fp = "./data/t_joint_data/"+tst_name+"/"+tst_name+".txt"
        sp = cls.from_catman_file(fp)

        # Assign the channels to a variable for easier access
        chn = sp.channels

        # Offset the Time channel so that it starts from 0
        sp.offset_channel_values(
            "Time",
            -sp.channels['Time']['data'][0]
        )

        # Trim the tail of the experiment from the same timestamp that the DIC
        # values were trimmed to remove the unloading part.
        final_pos = (
            np.abs(chn['Time']['data'] - dchn['WindowsTime']['data'][-1])
        ).argmin()
        for i in chn.values():
            i['data'] = i['data'][:final_pos]

        ## Remove the unloading part by deleting all the data after the max
        ## displacement point.
        #final_pos = chn['Displacement']['data'].argmax()
        #for i in chn.values():
        #    i['data'] = i['data'][:final_pos]

        # Downsample all of the catman channels so that they match the number of
        # samples of the DIC.
        sp.downsample_all(len(dchn['Fid']['data']))

        # Invert the Load channel
        sp.invert_sign('Load')

        # Rescale the measurements from the strain gauges and change the unit
        # labels.
        for i in ("B1", "B2", "C1", "C2", "C3", "C4", "C5", "C6"):
            chn[i]["data"] = chn[i]["data"]/1e6
            chn[i]["units"] = "m/m"

        # Correction for specimen C1B10M90. They have connected wrongly the two
        # beam strain gauges, B1 and B2. They have to be swapped.
        if tst_name == 'C1B10M90':
            chn['B11'] = chn.pop('B2')
            chn['B2'] = chn.pop('B1')
            chn['B1'] = chn.pop('B11')

        # Correction of the stran guage values of C2B10M90. Tare init.
        if tst_name == 'C2B10M90':
            chn['B1']['data'] = chn['B1']["data"] + 495.23/1e6
            chn['B2']["data"] = chn['B2']["data"] + 962.60/1e6

        # The displacement channel has the position of the piston. In order to get
        # the relative displacements with a zero point at the starting position of
        # the experiment, the starting position has to be excluded from the
        # recorded values. To do that, the position of the first Load reading that
        # exceeds 2kN is found and then the position of the piston at that moment
        # is excluded from the channel.
        init_pos = chn['Displacement']['data'][
            np.where(chn['Load']['data']>0.85)[0][0]
        ]
        chn['Displacement']['data'] = chn['Displacement']['data'] - init_pos

        # Calculate the moment in the beam by the readings of the pair of strain
        # gauges.
        sp.add_new_channel_zeros('M_ip,1', 'kNm', 'M_{ip,1,Ed}')
        chn["M_ip,1"]["data"] = 210e3*sp_th.cs_props["beam"].moi_1*(
            chn["B1"]["data"] - chn["B2"]["data"]
        )/(sp_th.geometry["height_b"]*1e6)

        # Merge the DIC channels in the catman database.
        chn.update(dchn)

        # Store the dead weight.
        sp.dead_weight = sp_th.bc_loads["dead_weight[kg]"]

        # Store entire theoretical object with the calculations
        sp.props = sp_th

        # Calculate non-dimensional loading.
        sp.add_new_channel_zeros('n,exp', '-', '$n_{chord}$')
        sp.channels["n,exp"]["data"] = sp.channels["Load"]["data"]/(
            sp_th.struct_props['n_b_rd']/1000
        )

        return(sp)

    def plot_disp_load(self, **kargs):
        """
        Plot the Load-Displacement curve.

        Parameters
        ----------
        **kargs : redirected to :obj:`PySS.lab_tests.Experiment.plot2d` method.

        Return
        ------
        :obj:`matplotlib.axes._subplots.AxesSubplot`

        """
        ax = self.plot2d("Displacement", "Load", **kargs)

        return(ax)

    def plot_sg_pairs(self, **kargs):
        """
        Plot curves of opposite strain-gauge pairs against the applies
        compressive load. Three strain-gauge pairs are used on each specimen,
        one on either side of the column (upper and lower) and one on the beam.
        The strain-gauge pairs are located on the opposing faces of the RHS
        profiles so that they monitor the in-plane bending.

        Parameters
        ----------
        **kargs : redirected to :obj:`PySS.lab_tests.Experiment.plot2d` method.

        Return
        ------
        tuple of 3x :obj:`matplotlib.axes._subplots.AxesSubplot`

        """
        ax1 = self.multiplot2d(
            ["C1", "C2"],
            ["Load", "Load"],
            legend_from="x"
        )

        ax2 = self.multiplot2d(
            ["C3", "C4"],
            ["Load", "Load"],
            legend_from="x"
        )

        ax3 = self.multiplot2d(
            ["B1", "B2"],
            ["Load", "Load"],
            legend_from="x"
        )

        return(ax1, ax2, ax3)

    def plot_head_displacements(self, **kargs):
        """
        Plot the in-plane and out-of-plane displacements of the coloumn head,
        as recorded by the two lase extensometers.

        Parameters
        ----------
        **kargs : redirected to :obj:`PySS.lab_tests.Experiment.plot2d` method.

        Return
        ------
        tuple of 3x :obj:`matplotlib.axes._subplots.AxesSubplot`

        """
        ax = self.multiplot2d(
            [
                "Lateral out-of-plane head displacement",
                "Lateral in-plane head displacement"
            ],
            ["Load", "Load"],
            legend_from="x"
        )

        return(ax)

    def plot_rotations(self, **kargs):
        """
        Plot the rotations of the beam and the coloumn as recorded by the two
        inclinometers. The location of the inclinometers on the different
        experiments alter, so they cannot be directly compared.

        Parameters
        ----------
        **kargs : redirected to :obj:`PySS.lab_tests.Experiment.plot2d` method.

        Return
        ------
        tuple of 3x :obj:`matplotlib.axes._subplots.AxesSubplot`

        """
        ax = self.multiplot2d(
            ["Beam rotation", "Column rotation"],
            ["Load", "Load"],
            legend_from="x"
        )

        return(ax)


class TestCollection(dict):
    """ Collection of experiments"""
    def __init__(self):
        pass

    @classmethod
    def from_batch(
        cls,
        batch=None
    ):
        tst = cls()

        if batch == 1:
            names = (
                'C1B10M70',
                'C1B09M90',
            )

            # Load all the experiments.
            for i in names:
                chk = TestData.from_1st_batch(i)
                tst.update({i: chk})

        elif batch == 2:
            # Tuple with the names of the 2nd batch experiments
            names = (
                'C1B10M90',
                'C1B09M70',
                'C2B10M70',
                'C2B10M90',
                'C2B09M70',
                'C2B09M90'
            )

            # Load all the experiments.
            for i in names:
                chk =  TestData.from_2nd_batch(i)
                tst.update({i: chk})

        elif not batch:
            tst = cls.from_batch(1)
            tst.update(cls.from_batch(2))
            #tst._sort_()

        return(tst)

    def _sort_(self):
        """
        Short the dictionary by the keys.
        """

        # Sort the items in the collection.
        self = {k: self[k] for k in sorted(self)}

    def plot_all(
        self,
        x_values,
        y_values,
        **kargs
    ):
        """ Plot specific channels from all the tests"""
        # Check if a single channel value is requested per specimen or multiple
        if isinstance(x_values, (list, tuple, np.ndarray)):
            plot_func = 'multiplot2d'
        elif isinstance(x_values, str):
            plot_func = 'plot2d'

        # Create groups of specimens by reshaping the dictionary keys. This
        # grouping is going to be used for putting the plots in pairs.
        groups = np.reshape(list(sorted(self.keys())), [len(self.keys())//2, 2])

        # Create empty lists for storing the generated plots and their axes.
        fig = []
        axs = []

        # Loop through the specimen pairs.
        for i, pair in enumerate(groups):
            # Create the figure with 2 subplots.
            figz, axz = plt.subplots(
                1,
                2,
                sharey='row',
                gridspec_kw={'wspace': 0}
            )

            # Add the current figures and axes to the list
            fig.append(figz)
            axs.append(axz)

            for side in (0, 1):
                # Produce the current plots.
                self[groups[i][side]].__getattribute__(plot_func)(
                    x_values,
                    y_values,
                    ax=axs[i][side],
                    axlabels=False,
                    #color='k',
                    **kargs
                )

                # Add titles for the current 2 plots.
                axs[i][side].set_title(groups[i][side])

            for ax in axs[i].flat:
                ax.label_outer()

            # Add big axes (helps setting a single x-label)
            ax = figz.add_subplot(111, frameon=False)
            plt.tick_params(
                labelcolor='none',
                top='off',
                bottom='off',
                left='off',
                right='off'
            )

            # Add common labels
            if isinstance(x_values, (list, tuple, np.ndarray)):
                ax.set_xlabel(
                    self[groups[i][0]].channels[x_values[0]]['type'] + ' [' +\
                    self[groups[i][0]].channels[x_values[0]]['units'] + ']'
                )
                ax.set_ylabel(
                    self[groups[i][0]].channels[y_values[0]]['type'] + ' [' +\
                    self[groups[i][0]].channels[y_values[0]]['units'] + ']'
                )
            elif isinstance(x_values, str):
                ax.set_xlabel(
                    self[groups[i][0]].channels[x_values]['type'] + ' [' +\
                    self[groups[i][0]].channels[x_values]['units'] + ']'
                )
                ax.set_ylabel(
                    self[groups[i][0]].channels[y_values]['type'] + ' [' +\
                    self[groups[i][0]].channels[y_values]['units'] + ']'
                )


            #ax.set_xlabel(self[groups[i][0]].channels[x_values]['type'])
            #ax.set_ylabel(self[groups[i][0]].channels[y_values]['type'])

        return(axs)

    def plot_beam_sgs(self):
        ax = self.plot_all(['Time', 'Time'], ['B1', 'B2'], legend_from='y')
        for cax in ax:
            cax[0].ticklabel_format(
                axis='y',
                style='sci',
                scilimits=(0,0)
            )

        plt.show(block=False)
        return(ax)

    def plot_beam_moments(self):
        """
        The beam moments on the experiments assessed through the strain gauges
        and the dead load.
        """
        # Create lists of names for the group-plotting and empty lists to
        # store the figures and axes created in the process.
        groups = np.reshape(list(sorted(self.keys())), [len(self.keys())//2, 2])

        fig = []
        axs = []

        # Some constants
        l_gauges = 361
        gggg = 9.81

        # Loop through the specimen pairs.
        for i, pair in enumerate(groups):
            # Create the figure with 2 subplots.
            figz, axz = plt.subplots(
                1,
                2,
                sharey='row',
                gridspec_kw={'wspace': 0}
            )

            # Add the current figures and axes to the list
            fig.append(figz)
            axs.append(axz)

            # Produce the current plots.
            self[groups[i][0]].plot2d(
                'Time',
                'M_ip,1',
                ax=axs[i][0],
                axlabels=False,
                color='k'
            )
            self[groups[i][1]].plot2d(
                'Time',
                'M_ip,1',
                ax=axs[i][1],
                axlabels=False,
                color='k'
            )

            # add horizontal lines at the exact values from the dead load
            axs[i][0].plot(
                [0, self[groups[i][0]].channels['Time']['data'][-2]],
                [
                    self[groups[i][0]].dead_weight*l_gauges*gggg/1e6,
                    self[groups[i][0]].dead_weight*l_gauges*gggg/1e6
                ],
                ':k'
            )
            axs[i][1].plot(
                [0, self[groups[i][1]].channels['Time']['data'][-2]],
                [
                    self[groups[i][1]].dead_weight*l_gauges*gggg/1e6,
                    self[groups[i][1]].dead_weight*l_gauges*gggg/1e6
                ],
                ':k'
            )

            for ax in axs[i].flat:
                ax.label_outer()

            # Add big axes (helps setting a single x-label)
            ax = figz.add_subplot(111, frameon=False)
            plt.tick_params(
                labelcolor='none',
                top='off',
                bottom='off',
                left='off',
                right='off'
            )

            # Add titles and legends for the current 2 plots.
            axs[i][0].set_title(groups[i][0])
            axs[i][0].legend(['SG pair', 'From dead-load'])
            axs[i][1].set_title(groups[i][1])

            # Add common labels
            ax.set_xlabel('$t$ [s]')
            ax.set_ylabel('$M_{ip,1,Ed}$ [kNm]')

        return(axs)

    def plot_disp_load(self):
        """Plot load displacement curves"""
        ax = self.plot_all('Displacement', 'Load')
        return(ax)

    def plot_normalised_load(self):
        """
        Plot the normalised load - displacement curves
        """
        # Create lists of names for the group-plotting and empty lists to
        # store the figures and axes created in the process.
        groups = np.reshape(
            list(sorted(self.keys())),
            [len(self.keys())//2, 2]
        )

        fig = []
        axs = []

        # Loop through the specimen pairs.
        for i, pair in enumerate(groups):
            # Create the figure with 2 subplots.
            figz, axz = plt.subplots(
                1,
                2,
                sharey='row',
                gridspec_kw={'wspace': 0}
            )

            # Add the current figures and axes to the list
            fig.append(figz)
            axs.append(axz)

            # Produce the current plots.
            self[groups[i][0]].plot2d(
                'Displacement',
                'n,exp',
                ax=axs[i][0],
                axlabels=False,
                color='k'
            )
            self[groups[i][1]].plot2d(
                'Displacement',
                'n,exp',
                ax=axs[i][1],
                axlabels=False,
                color='k'
            )

            # add horizontal lines at the exact values from the dead load
            axs[i][0].plot(
                [0, self[groups[i][0]].channels['Displacement']['data'][-2]],
                [
                    -self[groups[i][0]].props.struct_props['n_prc_col'],
                    -self[groups[i][0]].props.struct_props['n_prc_col'],
                ],
                ':k'
            )
            axs[i][1].plot(
                [0, self[groups[i][1]].channels['Displacement']['data'][-2]],
                [
                    -self[groups[i][1]].props.struct_props['n_prc_col'],
                    -self[groups[i][1]].props.struct_props['n_prc_col'],
                ],
                ':k'
            )

            for ax in axs[i].flat:
                ax.label_outer()

            # Add big axes (helps setting a single x-label)
            ax = figz.add_subplot(111, frameon=False)
            plt.tick_params(
                labelcolor='none',
                top='off',
                bottom='off',
                left='off',
                right='off'
            )

            # Add titles and legends for the current 2 plots.
            axs[i][0].set_title(groups[i][0])
            axs[i][0].legend(['SG pair', 'From dead-load'])
            axs[i][1].set_title(groups[i][1])

            # Add common labels
            ax.set_xlabel('$u_actuator$ [mm]')
            ax.set_ylabel('$n_chord$ [-]')


class Coupons(dict):
    """
    Coupon test class for the RHS-T-joints set of experiments.

    """
    def __init__(self):
        pass

    def plot_all_stress_strain(self):
        """
        Plot all true stress strain curves of the RHS-t-joint related coupon
        tests.
        """
        # Create lists of names for the group-plotting and empty lists to
        # store the figures and axes created in the process.
        #groups = np.reshape(list(sorted(self.keys())), [len(self.keys())//2, 2])
        groups = [
            [
                ["A1(opp)", "A2(opp)", "A3(opp)", "A4(adj)"],
                ["B1(adj)", "B2(opp)", "B3(adj)"]
            ],
            [
                ["C1(adj)", "C2(opp)", "C3(adj)"],
                ["D1(adj)", "D2(opp)", "D3(adj)"],
                ["E1(adj)", "E2(opp)", "E3(adj)"]
            ]
        ]

        fig = []
        axs = []

        # Some constants
        l_gauges = 361
        gggg = 9.81

        # Loop through the specimen pairs.
        for i, pair in enumerate(groups):
            # Create the figure with 2 subplots.
            figz, axz = plt.subplots(
                1,
                len(pair),
                sharey='row',
                gridspec_kw={'wspace': 0}
            )

            # Add the current figures and axes to the list
            fig.append(figz)
            axs.append(axz)

            # Produce the current plots.
            for n, profile in enumerate(pair):
                # Add titles and legends for the current 2 plots.
                axs[i][n].set_title(groups[i][n][0][0])
                #axs[i][n].legend(["chk", "this", "out"])

                for coupon in profile:
                    # Decide if it is an adjacent or opposite side coupon
                    if coupon[3] == "a":
                        style = "-"
                    else:
                        style = "--"

                    self[coupon].plot2d(
                        'Strain',
                        'Stress',
                        ax=axs[i][n],
                        axlabels=False,
                        color='k',
                        linestyle=style
                    )
                axs[i][n].legend(groups[i][n])
            #self[groups[i][1]].plot2d(
            #    'Time',
            #    'M_ip,1',
            #    ax=axs[i][1],
            #    axlabels=False,
            #    color='k'
            #)
#
#            # add horizontal lines at the exact values from the dead load
#            axs[i][0].plot(
#                [0, self[groups[i][0]].channels['Time']['data'][-2]],
#                [
#                    self[groups[i][0]].dead_weight*l_gauges*gggg/1e6,
#                    self[groups[i][0]].dead_weight*l_gauges*gggg/1e6
#                ],
#                ':k'
#            )
#            axs[i][1].plot(
#                [0, self[groups[i][1]].channels['Time']['data'][-2]],
#                [
#                    self[groups[i][1]].dead_weight*l_gauges*gggg/1e6,
#                    self[groups[i][1]].dead_weight*l_gauges*gggg/1e6
#                ],
#                ':k'
#            )

            #for ax in axs[i].flat:
            #    ax.label_outer()

            # Add big axes (helps setting a single x-label)
            ax = figz.add_subplot(111, frameon=False)
            plt.tick_params(
                labelcolor='none',
                top='off',
                bottom='off',
                left='off',
                right='off'
            )

            # Add common labels
            ax.set_xlabel(r'$\varepsilon$ [m/m]')
            ax.set_ylabel(r'$\sigma$ [MPa]')

        return(axs)



    @classmethod
    def from_file(cls):
        coupons = cls()
        for name in (
            "A1(opp)",
            "A2(opp)",
            "A3(opp)",
            "A4(adj)",
            "B1(adj)",
            "B2(opp)",
            "B3(adj)",
            "C1(adj)",
            "C2(opp)",
            "C3(adj)",
            "D1(adj)",
            "D2(opp)",
            "D3(adj)",
            "E1(adj)",
            "E2(opp)",
            "E3(adj)",

        ):
            coupons.update(
                {
                    name: lt.CouponTest.from_file(
                        "./data/t_joint_data/coupon_tests/"+name+".csv",
                        delimiter=","
                    )
                }
            )
        return(coupons)


def C1B10M70(use_nominal=False):
    # The first value is nominal, the second is measured.
    values = {
        "c_width": (100, 100.1),
        "c_f_y": (355, 463.668481021638),
        "c_f_u": (510, 541.),
        "c_nd_width": (25.8107, 32.077363097804835),
        "b_f_y": (355, 463.668481021638),
        "b_f_u": (510, 541.),
        "b_nd_width": (25.8107, 32.077363097804835),
        "m_prc": (0.5786, 0.7041)
    }

    if use_nominal:
        idx = 0
        dj.USE_NOMINAL = True
    else:
        idx = 1
        dj.USE_NOMINAL = False

    # The fy is calculated as (3*fy_adj+fy_opp)/4
    material_col = sd.Material(
        210000.,
        0.3,
        values["c_f_y"][idx],
        # Engineering f_u
        f_u_nominal=values["c_f_u"][idx]
        # True f_u
        #f_u_nominal=576.760910892975
    )
    material_beam = sd.Material(
        210000.,
        0.3,
        values["b_f_y"][idx],
        # Engineering f_u
        f_u_nominal=values["b_f_u"][idx]
        # True f_u
        #f_u_nominal=576.760910892975
    )

    sp_exp = dj.TjointRHS.from_slend_beta_m_prc_jj(
        values["c_nd_width"][idx],
        values["c_width"][idx],
        values["b_nd_width"][idx],
        1,
        values["m_prc"][idx],
        n_prc_beam=0,
        material_col=material_col,
        material_beam=material_beam,
        length_c=1102,
        length_b=511
    )

    # Store the applied dead load.
    sp_exp.bc_loads = {"dead_weight[kg]": 609.}

#    thick = 3.73
#    c_width = 100.1
#    b_width = 100.1
#    r_out = 2*thick
#    column = (
#        c_width,
#        c_width,
#        thick,
#    )
#
#    beam = (
#        b_width,
#        b_width,
#        thick,
#    )
#
#   sp_n0 = dj.TjointRHS.from_geometry(
#       column,
#       beam,
#       material_col=material_col,
#       material_beam=material_beam,
#       n_prc_col=0.,
#       n_prc_beam=0.,
#       length_c=1102,
#       length_b=511,
#       production_type="cold formed"
#   )
#
#   sp_n1 = dj.TjointRHS.from_geometry(
#       column,
#       beam,
#       material_col=material_col,
#       material_beam=material_beam,
#       n_prc_col=-1.,
#       n_prc_beam=0.,
#       length_c=1102,
#       length_b=511,
#       production_type="cold formed"
#   )
#
#   sp_exp = dj.TjointRHS.from_geometry(
#       column,
#       beam,
#       material_col=material_col,
#       material_beam=material_beam,
#       n_prc_col=-1.,
#       n_prc_beam=0.,
#       length_c=1102,
#       length_b=511,
#       production_type="cold formed"
#   )
#   return([sp_n0, sp_n1, sp_exp])
    return(sp_exp)


def C1B09M90(use_nominal=False):
    # The first value is nominal, the second is measured.
    values = {
        "c_width": (100, 100.1),
        "c_f_y": (355, 463.668481021638),
        "c_f_u": (510, 541.),
        "c_nd_width": (25.8107, 32.077363097804835),
        "b_f_y": (355, 434.324476123077),
        "b_f_u": (510, 500.),
        "b_nd_width": (22.738, 20.6236),
        "m_prc": (0.8028, 0.82916)
    }

    if use_nominal:
        idx = 0
        dj.USE_NOMINAL = True
    else:
        idx = 1
        dj.USE_NOMINAL = False

    # The fy is calculated as (3*fy_adj+fy_opp)/4
    material_col = sd.Material(
        210000.,
        0.3,
        values["c_f_y"][idx],
        # Engineering f_u
        f_u_nominal=values["c_f_u"][idx]
        # True f_u
        #f_u_nominal=576.760910892975
    )
    # For this beam, there is no opposite and adjacent measurements.
    material_beam = sd.Material(
        210000.,
        0.3,
        values["b_f_y"][idx],
        # Engineering f_u
        f_u_nominal=values["b_f_u"][idx],
        # True f_u
        #f_u_nominal=564.0840393
    )

#    c_width = 100.1
#    c_thick = 3.73
#    b_width = 90.
#    b_thick = 4.7
#    column = (
#        c_width,
#        c_width,
#        c_thick,
#    )
#
#    beam = (
#        b_width,
#        b_width,
#        b_thick,
#    )
#
#    sp_n0 = dj.TjointRHS.from_geometry(
#        column,
#        beam,
#        material_col=material_col,
#        material_beam=material_beam,
#        n_prc_col=0.,
#        n_prc_beam=0.,
#        length_c=1102,
#        length_b=511,
#        production_type="cold formed"
#    )
#
#    sp_n1 = dj.TjointRHS.from_geometry(
#        column,
#        beam,
#        material_col=material_col,
#        material_beam=material_beam,
#        n_prc_col=-1.,
#        n_prc_beam=0.,
#        length_c=1102,
#        length_b=511,
#        production_type="cold formed"
#    )
#
    sp_exp = dj.TjointRHS.from_slend_beta_m_prc_jj(
        values["c_nd_width"][idx],
        values["c_width"][idx],
        values["b_nd_width"][idx],
        0.9, #beta
        values["m_prc"][idx], # m_prc for engineering f_u
        #0.9328, # m_prc for true f_u
        n_prc_beam=0,
        material_col=material_col,
        material_beam=material_beam,
        length_c=1102,
        length_b=511
    )

    # Store the applied dead load.
    sp_exp.bc_loads = {"dead_weight[kg]": 876.4}

    #return([sp_n0, sp_n1, sp_exp])
    return(sp_exp)


# ---------------------------
def C1B10M90(use_nominal=False):
    # The first value is nominal, the second is measured.
    values = {
        "c_width": (100, 100.1),
        "c_f_y": (355, 545.397),
        "c_f_u": (510, 638),
        "c_nd_width": (25.8107, 31.13658387515693),
        "b_f_y": (355, 545.397),
        "b_f_u": (510, 638),
        "b_nd_width": (25.8107, 31.13658387515693),
        "m_prc": (0.9003, 0.9)
    }

    if use_nominal:
        idx = 0
        dj.USE_NOMINAL = True
    else:
        idx = 1
        dj.USE_NOMINAL = False

    material_col = sd.Material(
        210000.,
        0.3,
        values["c_f_y"][idx],
        # Engineering f_u
        f_u_nominal = values["c_f_u"][idx]
        # True f_u
        #f_u_nominal=700
    )
    material_beam = sd.Material(
        210000.,
        0.3,
        values["b_f_y"][idx],
        f_u_nominal = values["b_f_u"][idx]
        # True f_u
        #f_u_nominal=700
    )

    sp_exp = dj.TjointRHS.from_slend_beta_m_prc_jj(
        values["c_nd_width"][idx],
        values["c_width"][idx],
        values["b_nd_width"][idx],
        1.,
        values["m_prc"][idx],
        n_prc_beam=0,
        material_col=material_col,
        material_beam=material_beam,
        length_c=800,
        length_b=511
    )

    # Store the applied dead load.
    sp_exp.bc_loads = {"dead_weight[kg]": 948.}

#    c_width = 100.1
#    c_thick = 4.096
#    b_width = 100.1
#    b_thick = 4.096
#
#    column = (
#        c_width,
#        c_width,
#        c_thick,
#    )
#
#    beam = (
#        b_width,
#        b_width,
#        b_thick,
#    )
#
#    sp_n0 = dj.TjointRHS.from_geometry(
#        column,
#        beam,
#        material_col=material_col,
#        material_beam=material_beam,
#        n_prc_col=0.,
#        n_prc_beam=0.,
#        length_c=800,
#        length_b=511,
#        production_type="cold formed"
#    )
#
#    sp_n1 = dj.TjointRHS.from_geometry(
#        column,
#        beam,
#        material_col=material_col,
#        material_beam=material_beam,
#        n_prc_col=-1.,
#        n_prc_beam=0.,
#        length_c=800,
#        length_b=511,
#        production_type="cold formed"
#    )
#
    #return([sp_n0, sp_n1, sp_exp])
    return(sp_exp)


def C1B09M70(use_nominal=False):
    # The first value is nominal, the second is measured.
    values = {
        "c_width": (100, 100.1),
        "c_f_y": (355, 545.397),
        "c_f_u": (510, 638),
        "c_nd_width": (25.8107, 31.13658387515693),
        "b_f_y": (355, 505.397),
        "b_f_u": (510, 564),
        "b_nd_width": (22.738, 28.50044825826861),
        "m_prc": (0.7, 0.7)
    }

    if use_nominal:
        idx = 0
        dj.USE_NOMINAL = True
    else:
        idx = 1
        dj.USE_NOMINAL = False

    material_col = sd.Material(
        210000.,
        0.3,
        values["c_f_y"][idx],
        # Engineering f_u
        f_u_nominal = values["c_f_u"][idx]
        # True f_u
        #f_u_nominal=700
    )
    material_beam = sd.Material(
        210000.,
        0.3,
        values["b_f_y"][idx],
        # Engineering f_u
        f_u_nominal=values["b_f_u"][idx]
        #f_u_nominal=610.
    )

    sp_exp = dj.TjointRHS.from_slend_beta_m_prc_jj(
        values["c_nd_width"][idx],
        values["c_width"][idx],
        values["b_nd_width"][idx],
        0.9,
        values["m_prc"][idx],
        n_prc_beam=0,
        material_col=material_col,
        material_beam=material_beam,
        length_c=800,
        length_b=511
    )

    # Store the applied dead load.
    sp_exp.bc_loads = {"dead_weight[kg]": 926}

#    c_width = 100.1
#    c_thick = 4.096
#    b_width = 90.1
#    b_thick = 3.844
#
#    column = (
#        c_width,
#        c_width,
#        c_thick,
#    )
#
#    beam = (
#        b_width,
#        b_width,
#        b_thick,
#    )
#
#    sp_n0 = dj.TjointRHS.from_geometry(
#        column,
#        beam,
#        material_col=material_col,
#        material_beam=material_beam,
#        n_prc_col=0.,
#        n_prc_beam=0.,
#        length_c=800,
#        length_b=511,
#        production_type="cold formed"
#    )
#
#    sp_n1 = dj.TjointRHS.from_geometry(
#        column,
#        beam,
#        material_col=material_col,
#        material_beam=material_beam,
#        n_prc_col=-1.,
#        n_prc_beam=0.,
#        length_c=800,
#        length_b=511,
#        production_type="cold formed"
#    )
#
    #return([sp_n0, sp_n1, sp_exp])
    return(sp_exp)


def C2B10M70(use_nominal=False):
    # The first value is nominal, the second is measured.
    values = {
        "c_width": (100, 100.1),
        "c_f_y": (355, 537.815),
        "c_f_u": (510, 614.),
        "c_nd_width": (36.053, 45.00462938232863),
        "b_f_y": (355, 545.397),
        "b_f_u": (510, 638),
        "b_nd_width": (25.8107, 31.13658387515693),
        "m_prc": (0.7, 0.7)
    }

    if use_nominal:
        idx = 0
        dj.USE_NOMINAL = True
    else:
        idx = 1
        dj.USE_NOMINAL = False

    material_col = sd.Material(
        210000.,
        0.3,
        values["c_f_y"][idx],
        # Engineering f_u
        f_u_nominal=values["c_f_u"][idx]
        # True f_u
        #f_u_nominal=660.
    )
    material_beam = sd.Material(
        210000.,
        0.3,
        values["b_f_y"][idx],
        f_u_nominal=values["b_f_u"][idx]
        # True f_u
        #f_u_nominal=700
    )

    sp_exp = dj.TjointRHS.from_slend_beta_m_prc_jj(
        values["c_nd_width"][idx],
        values["c_width"][idx],
        values["b_nd_width"][idx],
        1.,
        values["m_prc"][idx],
        n_prc_beam=0,
        material_col=material_col,
        material_beam=material_beam,
        length_c=800,
        length_b=511
    )

    # Store the applied dead load.
    sp_exp.bc_loads = {"dead_weight[kg]": 284.}

#    c_width = 100.1
#    c_thick = 2.966
#    b_width = 100.1
#    b_thick = 4.096
#
#    column = (
#        c_width,
#        c_width,
#        c_thick,
#    )
#
#    beam = (
#        b_width,
#        b_width,
#        b_thick,
#    )
#
#    sp_n0 = dj.TjointRHS.from_geometry(
#        column,
#        beam,
#        material_col=material_col,
#        material_beam=material_beam,
#        n_prc_col=0.,
#        n_prc_beam=0.,
#        length_c=800,
#        length_b=511,
#        production_type="cold formed"
#    )
#
#    sp_n1 = dj.TjointRHS.from_geometry(
#        column,
#        beam,
#        material_col=material_col,
#        material_beam=material_beam,
#        n_prc_col=-1.,
#        n_prc_beam=0.,
#        length_c=800,
#        length_b=511,
#        production_type="cold formed"
#    )
#
    #return([sp_n0, sp_n1, sp_exp])
    return(sp_exp)


def C2B10M90(use_nominal=False):
    # The first value is nominal, the second is measured.
    values = {
        "c_width": (100, 100.1),
        "c_f_y": (355, 537.815),
        "c_f_u": (510, 614.),
        "c_nd_width": (36.053, 45.00462938232863),
        "b_f_y": (355, 545.397),
        "b_f_u": (510, 638),
        "b_nd_width": (25.8107, 31.13658387515693),
        "m_prc": (0.9, 0.9)
    }

    if use_nominal:
        idx = 0
        dj.USE_NOMINAL = True
    else:
        idx = 1
        dj.USE_NOMINAL = False

    material_col = sd.Material(
        210000.,
        0.3,
        values["c_f_y"][idx],
        # Engineering f_u
        f_u_nominal=values["c_f_u"][idx]
        # True f_u
        #f_u_nominal=660.
    )
    material_beam = sd.Material(
        210000.,
        0.3,
        values["b_f_y"][idx],
        f_u_nominal=values["b_f_u"][idx]
        # True f_u
        #f_u_nominal=700
    )

    sp_exp = dj.TjointRHS.from_slend_beta_m_prc_jj(
        values["c_nd_width"][idx],
        values["c_width"][idx],
        values["b_nd_width"][idx],
        1.,
        values["m_prc"][idx],
        n_prc_beam=0,
        material_col=material_col,
        material_beam=material_beam,
        length_c=800,
        length_b=511
    )

    # Store the applied dead load.
    sp_exp.bc_loads = {"dead_weight[kg]": 365.}

#    c_width = 100.1
#    c_thick = 2.966
#    b_width = 100.1
#    b_thick = 4.096
#
#    column = (
#        c_width,
#        c_width,
#        c_thick,
#    )
#
#    beam = (
#        b_width,
#        b_width,
#        b_thick,
#    )
#
#    sp_n0 = dj.TjointRHS.from_geometry(
#        column,
#        beam,
#        material_col=material_col,
#        material_beam=material_beam,
#        n_prc_col=0.,
#        n_prc_beam=0.,
#        length_c=800,
#        length_b=511,
#        production_type="cold formed"
#    )
#
#    sp_n1 = dj.TjointRHS.from_geometry(
#        column,
#        beam,
#        material_col=material_col,
#        material_beam=material_beam,
#        n_prc_col=-1.,
#        n_prc_beam=0.,
#        length_c=800,
#        length_b=511,
#        production_type="cold formed"
#    )
#
    #return([sp_n0, sp_n1, sp_exp])
    return(sp_exp)


def C2B09M70(use_nominal=False):
    # The first value is nominal, the second is measured.
    values = {
        "c_width": (100, 100.1),
        "c_f_y": (355, 537.815),
        "c_f_u": (510, 614.),
        "c_nd_width": (36.053, 45.00462938232863),
        "b_f_y": (355, 505.147),
        "b_f_u": (510, 564),
        "b_nd_width": (22.738, 28.50044825826861),
        "m_prc": (0.7, 0.7)
    }

    if use_nominal:
        idx = 0
        dj.USE_NOMINAL = True
    else:
        idx = 1
        dj.USE_NOMINAL = False

    material_col = sd.Material(
        210000.,
        0.3,
        values["c_f_y"][idx],
        # Engineering f_u
        f_u_nominal=values["c_f_u"][idx]
        # True f_u
        #f_u_nominal=660.
    )
    material_beam = sd.Material(
        210000.,
        0.3,
        values["b_f_y"][idx],
        # Engineering f_u
        f_u_nominal=values["b_f_u"][idx]
        #f_u_nominal=610.
    )

    sp_exp = dj.TjointRHS.from_slend_beta_m_prc_jj(
        values["c_nd_width"][idx],
        values["c_width"][idx],
        values["b_nd_width"][idx],
        0.9,
        values["m_prc"][idx],
        n_prc_beam=0,
        material_col=material_col,
        material_beam=material_beam,
        length_c=800,
        length_b=511
    )

    # Store the applied dead load.
    sp_exp.bc_loads = {"dead_weight[kg]": 450.}

#    c_width = 100.1
#    c_thick = 2.966
#    b_width = 90.1
#    b_thick = 3.844
#
#    column = (
#        c_width,
#        c_width,
#        c_thick,
#    )
#
#    beam = (
#        b_width,
#        b_width,
#        b_thick,
#    )
#
#    sp_n0 = dj.TjointRHS.from_geometry(
#        column,
#        beam,
#        material_col=material_col,
#        material_beam=material_beam,
#        n_prc_col=0.,
#        n_prc_beam=0.,
#        length_c=800,
#        length_b=511,
#        production_type="cold formed"
#    )
#
#    sp_n1 = dj.TjointRHS.from_geometry(
#        column,
#        beam,
#        material_col=material_col,
#        material_beam=material_beam,
#        n_prc_col=-1.,
#        n_prc_beam=0.,
#        length_c=800,
#        length_b=511,
#        production_type="cold formed"
#    )
#

    #return([sp_n0, sp_n1, sp_exp])
    return(sp_exp)


def C2B09M90(use_nominal=False):
    # The first value is nominal, the second is measured.
    values = {
        "c_width": (100, 100.1),
        "c_f_y": (355, 537.815),
        "c_f_u": (510, 614.),
        "c_nd_width": (36.053, 45.00462938232863),
        "b_f_y": (355, 505.147),
        "b_f_u": (510, 564),
        "b_nd_width": (22.738, 28.50044825826861),
        "m_prc": (0.9, 0.9)
    }

    if use_nominal:
        idx = 0
        dj.USE_NOMINAL = True
    else:
        idx = 1
        dj.USE_NOMINAL = False

    material_col = sd.Material(
        210000.,
        0.3,
        values["c_f_y"][idx],
        # Engineering f_u
        f_u_nominal=values["c_f_u"][idx]
        # True f_u
        #f_u_nominal=660.
    )
    material_beam = sd.Material(
        210000.,
        0.3,
        values["b_f_y"][idx],
        # Engineering f_u
        f_u_nominal=values["b_f_u"][idx]
        #f_u_nominal=610.
    )

    sp_exp = dj.TjointRHS.from_slend_beta_m_prc_jj(
        values["c_nd_width"][idx],
        values["c_width"][idx],
        values["b_nd_width"][idx],
        0.9,
        values["m_prc"][idx],
        n_prc_beam=0,
        material_col=material_col,
        material_beam=material_beam,
        length_c=800,
        length_b=511
    )

    # Store the applied dead load.
    sp_exp.bc_loads = {"dead_weight[kg]": 579.}

#    c_width = 100.1
#    c_thick = 2.966
#    b_width = 90.1
#    b_thick = 3.844
#
#    column = (
#        c_width,
#        c_width,
#        c_thick,
#    )
#
#    beam = (
#        b_width,
#        b_width,
#        b_thick,
#    )
#
#    sp_n0 = dj.TjointRHS.from_geometry(
#        column,
#        beam,
#        material_col=material_col,
#        material_beam=material_beam,
#        n_prc_col=0.,
#        n_prc_beam=0.,
#        length_c=800,
#        length_b=511,
#        production_type="cold formed"
#    )
#
#    sp_n1 = dj.TjointRHS.from_geometry(
#        column,
#        beam,
#        material_col=material_col,
#        material_beam=material_beam,
#        n_prc_col=-1.,
#        n_prc_beam=0.,
#        length_c=800,
#        length_b=511,
#        production_type="cold formed"
#    )
#
    #return([sp_n0, sp_n1, sp_exp])
    return(sp_exp)


def load_dic_from_1st_batch(tst_name):
    sp2_dic = TestData.from_file(
        "./data/t_joint_data/"+tst_name+"/DIC/"+tst_name+".csv",
    )

    # Post processing of the 2D DIC readings from the targets.
    # --------------------------------------------------------

    # Invert all the vertical readings from the 2D DIC measurements to make
    # upwards positive so that it matches with the abaqus model positive
    # direction.
    for i in sp2_dic.channels:
        if i[0] == "v":
            sp2_dic.invert_sign(i)

    # Part 1: Vertical displacements.
    # +++++++++++++++++++++++++++++++
    # Calculate average axial strain based on the two most extreme targets, 0
    # and 14
    sp2_dic.add_new_channel_zeros(
        "avg_axial_strain",
        "mm/mm",
        "$\varepsilon_{avg}$"
    )
    sp2_dic.channels["avg_axial_strain"]["data"] = (
        sp2_dic.channels["v_0"]["data"] - sp2_dic.channels["v_14"]["data"]
    )/(460+460)

    # Calculate the vertical displacement of the base hinge, point "B", based
    # on the displacement of the lowermost target, 14, and the average axial
    # strain.
    sp2_dic.add_new_channel_zeros("v_B", "mm", "$v$")
    sp2_dic.channels["v_B"]["data"] = (
        sp2_dic.channels["avg_axial_strain"]["data"]*(460-545)
    ) + sp2_dic.channels["v_14"]["data"]

    # Part 2: Horizontal displacements
    # ++++++++++++++++++++++++++++++++
    # Calculate the rotation of the column close to the ends based on the
    # target triplets. The rotation of each end is calculated twice, once based
    # on the vertically aligned target once for the horizontal. Afterwards, the
    # average value is calculated.
    sp2_dic.add_new_channel_zeros("phi1_0", "rad", "$\phi$")
    sp2_dic.channels["phi1_0"]["data"] = np.arctan(
        (sp2_dic.channels["v_1"]["data"]-sp2_dic.channels["v_2"]["data"])/60
    )

    sp2_dic.add_new_channel_zeros("phi2_0", "rad", "$\phi$")
    sp2_dic.channels["phi2_0"]["data"] = np.arctan(
        (sp2_dic.channels["u_0"]["data"]-sp2_dic.channels["u_1"]["data"])/60
    )

    sp2_dic.add_new_channel_zeros("phi_0", "rad", "$\phi$")
    sp2_dic.channels["phi_0"]["data"] = (
        sp2_dic.channels["phi1_0"]["data"] +\
        sp2_dic.channels["phi2_0"]["data"]
    )/2

    sp2_dic.add_new_channel_zeros("phi1_14", "rad", "$\phi$")
    sp2_dic.channels["phi1_14"]["data"] = np.arctan(
        (sp2_dic.channels["u_13"]["data"]-sp2_dic.channels["u_14"]["data"])/60
    )

    sp2_dic.add_new_channel_zeros("phi2_14", "rad", "$\phi$")
    sp2_dic.channels["phi2_14"]["data"] = np.arctan(
        (sp2_dic.channels["v_12"]["data"]-sp2_dic.channels["v_13"]["data"])/60
    )

    sp2_dic.add_new_channel_zeros("phi_14", "rad", "$\phi$")
    sp2_dic.channels["phi_14"]["data"] = (
        sp2_dic.channels["phi1_14"]["data"] +\
        sp2_dic.channels["phi2_14"]["data"]
    )/2

    # Calculate the differential horizontal displacement between the two
    # extreme targets (0, 14) with respect to the two hinges (A, B), based on
    # the assumption that the rotation at the position of the targets is equal
    # to the rotation of the hinges.
    sp2_dic.add_new_channel_zeros("u_0,A", "mm", "$u$")
    sp2_dic.channels["u_0,A"]["data"] = np.tan(
        sp2_dic.channels["phi_0"]["data"]
    )*(557-460)

    sp2_dic.add_new_channel_zeros("u_14,B", "mm", "$u$")
    sp2_dic.channels["u_14,B"]["data"] = np.tan(
        sp2_dic.channels["phi_14"]["data"]
    )*(-545+460)

    # Then, exclude the horizontal displacements due to end rotations from the
    # total horizontal displacements of the extreme targets to get the
    # horizontal displacements of the hinges.
    sp2_dic.add_new_channel_zeros("u_A", "mm", "$u$")
    sp2_dic.channels["u_A"]["data"] = (
        sp2_dic.channels["u_0"]["data"]+sp2_dic.channels["u_0,A"]["data"]
    )

    sp2_dic.add_new_channel_zeros("u_B", "mm", "$u$")
    sp2_dic.channels["u_B"]["data"] = (
        sp2_dic.channels["u_14"]["data"]+sp2_dic.channels["u_14,B"]["data"]
    )

    # Now that the clean horizontal displacements of the two hinges are known,
    # the rigid body rotation of the entire column is calculated.
    sp2_dic.add_new_channel_zeros("theta", "rad", "$\phi$")
    sp2_dic.channels["theta"]["data"] = np.arctan(
        (
            sp2_dic.channels["u_A"]["data"]-sp2_dic.channels["u_B"]["data"]
        )/(557+545)
    )

    # Translate/rotate all 2D DIC
    # +++++++++++++++++++++++++++
    # Subtract the horizontal and vertical displacement of the base hinge,
    # that were just calculated, u_B and v_B, from the measured displacements
    # of all the targets . This way, the base hinge is set as the axes origin
    # for all 2D DIC measurements.
    for i in sp2_dic.channels.items():
        if i[0][0] == "u":
            i[1]["data"] = i[1]["data"] - sp2_dic.channels["u_14"]["data"]
        elif i[0][0] == "v":
            i[1]["data"] = i[1]["data"] - sp2_dic.channels["v_14"]["data"]

    # Remove horizontal displacements due to rigid body rotation from the
    # measured displacements of all the targets. This way, the theoretical axis
    # of the column is the vertical reference and all displacements measured
    # regard only to the specimen deformations, clean from boundary
    # displacements.
    z_locations = (
        460,
        400,
        400,
        150,
        150,
        0,
        25,
        -25,
        25,
        -25,
        -150,
        -150,
        -400,
        -400,
        -460
    )
    for n, i in enumerate(z_locations):
        sp2_dic.channels["u_"+str(n)]["data"] = sp2_dic.channels["u_"+str(n)]["data"]-(
            np.tan(sp2_dic.channels["theta"]["data"])*(545+i-85)
        )

    sp2_dic.channels["u_A"]["data"] = sp2_dic.channels["u_A"]["data"]-np.tan(
        sp2_dic.channels["theta"]["data"]
    )*(545+557-85)

    # Remove the rigid body rotations from the total measured rotations at the
    # extreme targets.
    sp2_dic.channels["phi_0"]["data"] = (
        sp2_dic.channels["phi_0"]["data"] - sp2_dic.channels["theta"]["data"]
    )

    sp2_dic.channels["phi_14"]["data"] = (
        sp2_dic.channels["phi_14"]["data"] - sp2_dic.channels["theta"]["data"]
    )

    # Extra calculations
    # ++++++++++++++++++
    # Calculate the rotation at the pairs of targets on the middle of the
    # column.
    sp2_dic.add_new_channel_zeros("phi_3,4", "rad", "$\phi$")
    sp2_dic.channels["phi_3,4"]["data"] = np.arctan(
        (sp2_dic.channels["v_3"]["data"]-sp2_dic.channels["v_4"]["data"])/60
    )

    sp2_dic.add_new_channel_zeros("phi_10,11", "rad", "$\phi$")
    sp2_dic.channels["phi_10,11"]["data"] = np.arctan(
        (sp2_dic.channels["v_10"]["data"]-sp2_dic.channels["v_11"]["data"])/60
    )

    # Calculate the rotation at the pairs of targets on the beam
    sp2_dic.add_new_channel_zeros("phi_8,9", "rad", "$\phi$")
    sp2_dic.channels["phi_8,9"]["data"] = np.arctan(
        (sp2_dic.channels["u_8"]["data"]-sp2_dic.channels["u_9"]["data"])/60
    )

    sp2_dic.add_new_channel_zeros("phi_7,6", "rad", "$\phi$")
    sp2_dic.channels["phi_7,6"]["data"] = np.arctan(
        (sp2_dic.channels["u_7"]["data"]-sp2_dic.channels["u_6"]["data"])/60
    )

    return(sp2_dic)


def load_t_joint_abq(path):
    """ Load abaqus data from the second specimen, 'C1B10M70'"""
    # Load values from exported history output requests (mostly displacements)
    ho = TestData.from_file(
        path + "/C1B09M90_job.hou"
    )

    # Rename channels to match the names of the channels from the tests
    ho.channels["Displacement"] = ho.channels.pop("Node ASSEMBLY.3_U3")
    ho.channels["Reaction"] = ho.channels.pop("Node ASSEMBLY.2_RF3")
    ho.channels["Load"] = ho.channels.pop("Node ASSEMBLY.3_CF3")
    ho.channels["Beam_load"] = ho.channels.pop("Node ASSEMBLY.1_CF3")

    ho.channels["v_0"] = ho.channels.pop("Node COLUMN_HIGH_SHELL_INST.7_U3")
    ho.channels["u_0"] = ho.channels.pop("Node COLUMN_HIGH_SHELL_INST.7_U1")

    ho.channels["v_1"] = ho.channels.pop("Node COLUMN_HIGH_SHELL_INST.5_U3")
    ho.channels["u_1"] = ho.channels.pop("Node COLUMN_HIGH_SHELL_INST.5_U1")

    ho.channels["v_2"] = ho.channels.pop("Node COLUMN_HIGH_SHELL_INST.1_U3")
    ho.channels["u_2"] = ho.channels.pop("Node COLUMN_HIGH_SHELL_INST.1_U1")

    ho.channels["v_12"] = ho.channels.pop(
        "Node COLUMN_LOW_SHELL_INST.37_U3"
    )
    ho.channels["u_12"] = ho.channels.pop(
        "Node COLUMN_LOW_SHELL_INST.37_U1"
    )

    ho.channels["v_13"] = ho.channels.pop(
        "Node COLUMN_LOW_SHELL_INST.36_U3"
    )
    ho.channels["u_13"] = ho.channels.pop(
        "Node COLUMN_LOW_SHELL_INST.36_U1"
    )

    ho.channels["v_14"] = ho.channels.pop(
        "Node COLUMN_LOW_SHELL_INST.40_U3"
    )

    ho.channels["u_14"] = ho.channels.pop(
        "Node COLUMN_LOW_SHELL_INST.40_U1"
    )

    ho.channels["v_6"] = ho.channels.pop("Node BEAM_SHELL_INST.7_U3")
    ho.channels["u_6"] = ho.channels.pop("Node BEAM_SHELL_INST.7_U1")

    ho.channels["v_7"] = ho.channels.pop("Node BEAM_SHELL_INST.9_U3")
    ho.channels["u_7"] = ho.channels.pop("Node BEAM_SHELL_INST.9_U1")

    ho.channels["v_8"] = ho.channels.pop("Node BEAM_SHELL_INST.10_U3")
    ho.channels["u_8"] = ho.channels.pop("Node BEAM_SHELL_INST.10_U1")

    ho.channels["v_9"] = ho.channels.pop("Node BEAM_SHELL_INST.8_U3")
    ho.channels["u_9"] = ho.channels.pop("Node BEAM_SHELL_INST.8_U1")

    ho.channels["v_w_bottom_col"] = ho.channels.pop(
        "Node COLUMN_INST.75_U3"
    )
    ho.channels["u_w_bottom_col"] = ho.channels.pop(
        "Node COLUMN_INST.75_U1"
    )

    ho.channels["v_w_top_col"] = ho.channels.pop(
        "Node COLUMN_INST.106_U3"
    )
    ho.channels["u_w_top_col"] = ho.channels.pop(
        "Node COLUMN_INST.106_U1"
    )

    ho.channels["v_w_bottom_beam"] = ho.channels.pop(
        "Node BEAM_INST.33_U3"
    )
    ho.channels["u_w_bottom_beam"] = ho.channels.pop(
        "Node BEAM_INST.33_U1"
    )

    ho.channels["v_w_top_beam"] = ho.channels.pop(
        "Node BEAM_INST.32_U3"
    )
    ho.channels["u_w_top_beam"] = ho.channels.pop(
        "Node BEAM_INST.32_U1"
    )

    # Post-process history output channels
    # ------------------------------------
    ## Invert the sign of the displacements to make positive downwards.
    #ho.invert_sign("Displacement")
    #for i in ho.channels:
    #    if i[0] == "v":
    #        ho.invert_sign(i)

    # Express loads in kN
    for i in ("Load", "Reaction"):
        ho.channels[i]["data"] = ho.channels[i]["data"]/1000

    # Subtract the horizontal and vertical displacement of target 14 (very
    # bottom) from all the 2D DIC data so that it becomes the axis origin.
    for i in ho.channels.items():
        if i[0][0] == "u":
            i[1]["data"] = i[1]["data"] - ho.channels["u_14"]["data"]
        elif i[0][0] == "v":
            i[1]["data"] = i[1]["data"] - ho.channels["v_14"]["data"]


    #ho.channels["Load"]["data"] = ho.channels["Load"]["data"]/1000
    #ho.channels["Reaction"]["data"] = ho.channels["Reaction"]["data"]/1000

    # Calculate weld deformations
    # +++++++++++++++++++++++++++
    ho.add_new_channel_zeros("u_diff_top", "mm", "$u$")
    ho.add_new_channel_zeros("v_diff_top", "mm", "$v$")
    ho.add_new_channel_zeros("weld_top_dist", "mm", "$\delta l$")
    ho.channels["u_diff_top"]["data"] = (
        ho.channels["u_w_top_beam"]["data"]
    ) - (
        ho.channels["u_w_top_col"]["data"]
    )
    ho.channels["v_diff_top"]["data"] = (
        ho.channels["v_w_top_beam"]["data"]
    ) - (
        ho.channels["v_w_top_col"]["data"]
    )
    ho.channels["weld_top_dist"]["data"] = np.sqrt(
        (
            ho.channels["u_diff_top"]["data"]**2
        ) + (
            ho.channels["v_diff_top"]["data"]**2
        )
    )

    ho.add_new_channel_zeros("u_diff_bot", "mm", "$u$")
    ho.add_new_channel_zeros("v_diff_bot", "mm", "$v$")
    ho.add_new_channel_zeros("weld_bot_dist", "mm", "$\delta l$")
    ho.channels["u_diff_bot"]["data"] = (
        ho.channels["u_w_bottom_beam"]["data"]
    ) - (
        ho.channels["u_w_bottom_col"]["data"]
    )
    ho.channels["v_diff_bot"]["data"] = (
        ho.channels["v_w_bottom_beam"]["data"]
    ) - (
        ho.channels["v_w_bottom_col"]["data"]
    )
    ho.channels["weld_bot_dist"]["data"] = np.sqrt(
        (
            ho.channels["u_diff_bot"]["data"]**2
        ) + (
            ho.channels["v_diff_bot"]["data"]**2
        )
    )

    # Calculate rotations
    # +++++++++++++++++++
    # Calculate the rotation of the column close to the ends based on the
    # target triplets. The rotation of each end is calculated twice, once based
    # on the vertically aligned target once for the horizontal. Afterwards, the
    # average value is calculated.
    ho.add_new_channel_zeros("phi1_0", "rad", "$\phi$")
    ho.channels["phi1_0"]["data"] = np.arctan(
        (ho.channels["v_1"]["data"]-ho.channels["v_2"]["data"])/60
    )

    ho.add_new_channel_zeros("phi2_0", "rad", "$\phi$")
    ho.channels["phi2_0"]["data"] = np.arctan(
        (ho.channels["u_0"]["data"]-ho.channels["u_1"]["data"])/60
    )

    ho.add_new_channel_zeros("phi_0", "rad", "$\phi$")
    ho.channels["phi_0"]["data"] = (
        ho.channels["phi1_0"]["data"] +\
        ho.channels["phi2_0"]["data"]
    )/2

    ho.add_new_channel_zeros("phi1_14", "rad", "$\phi$")
    ho.channels["phi1_14"]["data"] = np.arctan(
        (ho.channels["u_13"]["data"]-ho.channels["u_14"]["data"])/60
    )

    ho.add_new_channel_zeros("phi2_14", "rad", "$\phi$")
    ho.channels["phi2_14"]["data"] = np.arctan(
        (ho.channels["v_12"]["data"]-ho.channels["v_13"]["data"])/60
    )

    ho.add_new_channel_zeros("phi_14", "rad", "$\phi$")
    ho.channels["phi_14"]["data"] = (
        ho.channels["phi1_14"]["data"] +\
        ho.channels["phi2_14"]["data"]
    )/2

    ## Calculate the rotation at the pairs of targets on the middle of the
    ## column.
    #ho.add_new_channel_zeros("phi_3,4", "rad", "$\phi$")
    #ho.channels["phi_3,4"]["data"] = np.arctan(
    #    (ho.channels["v_3"]["data"]-ho.channels["v_4"]["data"])/60
    #)

    #ho.add_new_channel_zeros("phi_10,11", "rad", "$\phi$")
    #ho.channels["phi_10,11"]["data"] = np.arctan(
    #    (ho.channels["v_10"]["data"]-ho.channels["v_11"]["data"])/60
    #)

    # Calculate the rotation at the pairs of targets on the beam
    ho.add_new_channel_zeros("phi_8,9", "rad", "$\phi$")
    ho.channels["phi_8,9"]["data"] = np.arctan(
        (ho.channels["u_8"]["data"]-ho.channels["u_9"]["data"])/60
    )

    ho.add_new_channel_zeros("phi_7,6", "rad", "$\phi$")
    ho.channels["phi_7,6"]["data"] = np.arctan(
        (ho.channels["u_7"]["data"]-ho.channels["u_6"]["data"])/60
    )

    # Load values from data series from field output (logarithmic strains)
    fo = TestData.from_rpt(
        path + "/C1B09M90_job.rpt"
    )

    # Rename
    fo.channels["B1"] = fo.channels.pop(
        "LE:LE11 (Avg: 75p) SP:1 PI: BEAM_SHELL_INST N: 34"
    )
    fo.channels["B2"] = fo.channels.pop(
        "LE:LE11 (Avg: 75p) SP:1 PI: BEAM_SHELL_INST N: 35"
    )

    fo.channels["C1"] = fo.channels.pop(
        "LE:LE11 (Avg: 75p) SP:1 PI: COLUMN_HIGH_SHELL_INSTN: 26"
    )
    fo.channels["C2"] = fo.channels.pop(
        "LE:LE11 (Avg: 75p) SP:1 PI: COLUMN_HIGH_SHELL_INSTN: 29"
    )

    fo.channels["C3"] = fo.channels.pop(
        "LE:LE11 (Avg: 75p) SP:1 PI: COLUMN_LOW_SHELL_INST N: 65"
    )
    fo.channels["C4"] = fo.channels.pop(
        "LE:LE11 (Avg: 75p) SP:1 PI: COLUMN_LOW_SHELL_INST N: 59"
    )

    # Post-process
    fo.channels["X"]["data"] = np.insert(
        fo.channels["X"]["data"][1:].reshape(-1, 4).mean(axis=1),
        0,
        0
    )

    fo.channels["B1"]["data"] = np.insert(
        fo.channels["B1"]["data"][1:].reshape(-1, 4).mean(axis=1),
        0,
        0
    )

    fo.channels["B2"]["data"] = np.insert(
        fo.channels["B2"]["data"][1:].reshape(-1, 4).mean(axis=1),
        0,
        0
    )

    fo.channels["C1"]["data"] = np.insert(
        fo.channels["C1"]["data"][1:].reshape(-1, 4).mean(axis=1),
        0,
        0
    )

    fo.channels["C2"]["data"] = np.insert(
        fo.channels["C2"]["data"][1:].reshape(-1, 4).mean(axis=1),
        0,
        0
    )

    fo.channels["C3"]["data"] = np.insert(
        fo.channels["C3"]["data"][1:].reshape(-1, 4).mean(axis=1),
        0,
        0
    )

    fo.channels["C4"]["data"] = np.insert(
        fo.channels["C4"]["data"][1:].reshape(-1, 4).mean(axis=1),
        0,
        0
    )

    for i in fo.channels:
        fo.channels[i]["type"] = "$\\varepsilon_{11}$"
        fo.channels[i]["units"] = "mm/mm"

    # Post processing of strain gauge readings
    # ----------------------------------------
    # Create an RHS T-joint object with all the theoretical calculations
    # (properties, resistances etc.
    sp2_th = C1B09M90()[2]
    # Calculate the bending moment of the column at the locations of the strain
    # gauges. The following calculation is valid only as long as the profile
    # remains elastic.
    fo.add_new_channel_zeros("M_u,Ed", "Nm", "M")
    fo.channels["M_u,Ed"]["data"] = 210e6*sp2_th.cs_props["column"].moi_1*(
        fo.channels["C2"]["data"] - fo.channels["C1"]["data"]
    )/sp2_th.geometry["height_c"]

    fo.add_new_channel_zeros("M_d,Ed", "Nm", "M")
    fo.channels["M_d,Ed"]["data"] = 210e6*sp2_th.cs_props["column"].moi_1*(
        fo.channels["C4"]["data"] - fo.channels["C3"]["data"]
    )/sp2_th.geometry["height_c"]

    fo.add_new_channel_zeros("M_b,Ed", "Nm", "M")
    fo.channels["M_b,Ed"]["data"] = 210e6*sp2_th.cs_props["beam"].moi_1*(
        fo.channels["B2"]["data"] - fo.channels["B1"]["data"]
    )/sp2_th.geometry["height_b"]

    # After measuring manually the added moment on the column due to the dead
    # load at the beam, the distribution ratio between the upper and lower part
    # of the column is calculated, normalised to the bending moment at the
    # beam.
    mm_u_Ed = (659e6)/(-3265e6)
    mm_d_Ed = (-458e6)/(-3265e6)

    # Then, the moment in the column, at the two measured locations is adjusted
    # to exclude the bending from the dead load.
    fo.add_new_channel_zeros("M_N,u,Ed", "Nm", "M")
    fo.channels["M_N,u,Ed"]["data"] = fo.channels["M_u,Ed"]["data"] - (
        mm_u_Ed*fo.channels["M_b,Ed"]["data"]
    )

    fo.add_new_channel_zeros("M_N,d,Ed", "Nm", "M")
    fo.channels["M_N,d,Ed"]["data"] = fo.channels["M_d,Ed"]["data"] - (
        mm_d_Ed*fo.channels["M_b,Ed"]["data"]
    )

    # Finally, the moment at the position of the hinges is calculated by
    # extrapolating from the two known points, up and down.
    fo.add_new_channel_zeros("M_A,Ed", "Nm", "M")
    fo.channels["M_A,Ed"]["data"] = 157*(
        fo.channels["M_N,u,Ed"]["data"] - fo.channels["M_N,d,Ed"]["data"]
    )/800 + fo.channels["M_N,u,Ed"]["data"]

    fo.add_new_channel_zeros("M_B,Ed", "Nm", "M")
    fo.channels["M_B,Ed"]["data"] = -145*(
        fo.channels["M_N,u,Ed"]["data"] - fo.channels["M_N,d,Ed"]["data"]
    )/800 + fo.channels["M_N,d,Ed"]["data"]

    # Merge field and history outs to a single database
    ho.channels.update(fo.channels)
    return(ho)


def plot_in_all_sp(tests, x_values, y_values):
    """
    Produce a plot of a specific measurement from multiple tests.

    Parameters
    ----------
    tests : dict
        A dictionary populated with multiple :obj:`TestData`.
    x_values, y_values: str
        Name of the channels to be plotted on the x and y axes. The channels
        must be found in all the tests contained in the given dictionary.

    Return
    ------
    ax

    """
    # Create a figure and axes to plot on.
    ax = plt.axes()

    # Loop through all the tests and plot the requested measurements.
    for test in tests:
        tests[test].plot2d(x_values, y_values, ax=ax)

    ax.legend(tests.keys())

    return(ax)


def plot_interaction_and_experiment_results():
    """
    Produce plots with prEN interaction curves and the experiment results.
    """
    for thick in [3.73]:
        sp = []

        for nnnn, beta in enumerate([1, 0.9]):
            column = (
                100.,
                100.,
                thick,
            )

            beam = (
                beta*100.,
                beta*100.,
                thick,
            )

            #n_prc_col = 0.437
            n_prc_beam = 0.

            # Material from measured values
            material_col = sd.Material(210000., 0.3, 477.7)
            material_col.f_u_nominal = 560.
            material_beam = [
                sd.Material(210000., 0.3, 477.7),
                sd.Material(210000., 0.3, 435.)
            ]
            material_beam[0].f_u_nominal = 560.
            material_beam[1].f_u_nominal = 564.

            # Material from nominal values
            #material_col=sd.Material.from_nominal(nominal_strength="S355")
            #material_col.f_u_nominal = 510.
            #material_beam=sd.Material.from_nominal(nominal_strength="S355")
            #material_beam.f_u_nominal = 510.

            # Loop through n_prc values.
            sp.append([])
            n_values = 50
            for n_prc_col in np.linspace(0, -1, n_values):
                logging.info(
                    "Input values given: "
                )
                logging.info(
                    "Column: %.3f %.3f %.3f, %.3f" % (
                        column[0],
                        column[1],
                        column[2],
                        n_prc_col
                    )
                )
                logging.info(
                    "Beam: %.3f, %.3f, %.3f, %.3f\n" % (
                        beam[0],
                        beam[1],
                        beam[2],
                        n_prc_beam
                    )
                )

                sp[nnnn].append(
                    dj.TjointRHS.from_geometry(
                        column,
                        beam,
                        weld=None,
                        material_col=material_col,
                        material_beam=material_beam[nnnn],
                        n_prc_col=n_prc_col,
                        n_prc_beam=n_prc_beam,
                        length_c=1102.
                    )
                )

        # First plot
        fig1 = plt.figure()

        # EC interaction curves.
        plt.plot(
            np.linspace(0, 1, n_values),
            [i.struct_props["m_prc_j_pl"] for i in sp[0]],
            "b--",
            np.linspace(0, 1, n_values),
            [i.struct_props["m_prc_j_pl"] for i in sp[1]],
            "b:",
            np.linspace(0, 1, n_values),
            [i.struct_props["m_prc_j_pl_draft"] for i in sp[0]],
            "r--",
            np.linspace(0, 1, n_values),
            [i.struct_props["m_prc_j_pl_draft"] for i in sp[1]],
            "r:",
            np.linspace(0, 1, n_values),
            [i.struct_props["m_prc_m_pl"] for i in sp[0]],
            "k-",
        )

        # Points from experiments.
        plt.plot(1.082, 0.075, "ko", 1.050, 0.108, "kx")

        plt.legend(
            [
                "EN1993-1-8 $\\beta=1.0$",
                "EN1993-1-8 $\\beta=0.9$",
                "prEN1993-1-8 $\\beta=1.0$",
                "prEN1993-1-8 $\\beta=0.9$",
                "EN1993-1-1",
                "sp1, ($\\beta=1.0$)",
                "sp2, ($\\beta=0.9$)"
            ]
        )
        plt.grid(True)
        plt.xlabel(r"$\sfrac{N_{Ed}}{N_{pl,Rd,column}}$")
        plt.ylabel(r"$\sfrac{M_{Ed}}{M_{pl,Rd,column}}$")
        plt.tight_layout()

        plt.show(block=False)

        # Second plot
        fig2 = plt.figure()
        plt.plot(
            np.linspace(0, 1, n_values),
            [i.struct_props["m_prc_jj_draft"] for i in sp[0]],
            "k-",
            np.linspace(0, 1, n_values),
            [i.struct_props["m_prc_jj_draft"] for i in sp[1]],
            "k--",
        )

        plt.plot(1.082, 0.7, "ko", 1.0187, 0.823145, "kx")

        plt.legend(
            [
                "$\\beta=1.0$",
                "$\\beta=0.9$",
                "sp1, ($\\beta=1.0$)",
                "sp2, ($\\beta=0.9$)"
            ]
        )
        plt.grid(True)
        plt.xlabel("$\\sfrac{N_{Ed}}{N_{pl,Rd,column}}$")
        plt.ylabel("$\\sfrac{M_{Ed}}{M_{ip,Rd}}$")
        plt.tight_layout()

        plt.show(block=False)

    return(sp)


def load_theoretical_speciment(use_nominal=False):
    sps = [
        C1B10M70(use_nominal=use_nominal),
        C1B10M90(use_nominal=use_nominal),
        C1B09M70(use_nominal=use_nominal),
        C1B09M90(use_nominal=use_nominal),
        C2B10M70(use_nominal=use_nominal),
        C2B10M90(use_nominal=use_nominal),
        C2B09M70(use_nominal=use_nominal),
        C2B09M90(use_nominal=use_nominal),
    ]

    return(sps)


if __name__=="__main__":
    print("Blank main was executed")

