import PySS.polygonal as pg


def return_formatter(specimen):
    return_string = \
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f}"\
            .format(
                specimen.geometry.length/1000.,                             # meters
                specimen.geometry.thickness,                                # mm
                specimen.geometry.facet_flat_width,                         # mm
                specimen.geometry.r_circle,                                 # mm
                specimen.geometry.r_circumscribed,                          # mm
                specimen.cs_props.area,                                     # mm^2
                specimen.cs_props.moi_1,                                    # mm^4
                specimen.cs_props.a_eff,                                    # mm^2
                specimen.struct_props.lmbda_y,                              # -
                specimen.struct_props.p_classification,                     # -
                specimen.struct_props.sigma_cr_plate,                       # MPa
                specimen.struct_props.n_cr_plate/1000.,                     # kN
                specimen.struct_props.n_pl_rd/1000.,                        # kN
                specimen.struct_props.sigma_b_rd_plate,                     # MPa
                specimen.struct_props.n_b_rd_plate/1000.,                   # kN
                specimen.struct_props.t_classification,                     # -
                specimen.struct_props.lenca,                                 # -
                specimen.struct_props.sigma_cr_shell,                       # MPa
                specimen.struct_props.n_cr_shell/1000.,                     # kN
                specimen.struct_props.sigma_b_rd_shell,                     # MPa
                specimen.struct_props.n_b_rk_shell/1000,                    # kN
                specimen.struct_props.n_b_rd_shell/1000,                    # kN
                specimen.struct_props.lenca_new,                             # -
                specimen.struct_props.sigma_cr_shell_new,                   # MPa
                specimen.struct_props.n_cr_shell_new/1000.,                 # kN
                specimen.struct_props.sigma_b_rd_shell_new,                 # MPa
                specimen.struct_props.n_b_rd_shell_new/1000,                # kN
                (specimen.cs_props.moi_1 / specimen.cs_props.area) ** 0.5,
                (specimen.cs_props.moi_1 / specimen.cs_props.a_eff) ** 0.5,
                specimen.struct_props.n_b_rd_plate / specimen.struct_props.n_b_rd_shell,
                specimen.struct_props.sigma_b_rd_plate / specimen.material.f_y_nominal,
                specimen.struct_props.sigma_b_rd_shell / specimen.material.f_y_nominal,
                specimen.struct_props.sigma_b_rd_shell_new / specimen.material.f_y_nominal,
                specimen.struct_props.sigma_b_rd_shell / specimen.struct_props.sigma_b_rd_shell_new,
                specimen.cs_props.area * specimen.geometry.length * 8e-6
    )

    return return_string


def calc_from_length(
        n_sides,
        r_circle,
        p_classification,
        length,
        f_yield,
        fab_class,
        **kargs
):

    sp = pg.TheoreticalSpecimen.from_pclass_radius_length(
            n_sides,
            r_circle,
            p_classification,
            length,
            f_yield,
            fab_class)
    
    return return_formatter(sp)


def calc_from_flexslend(
        n_sides,
        r_circle,
        p_classification,
        lambda_flex,
        f_yield,
        fab_class,
        **kargs
):
    sp = pg.TheoreticalSpecimen.from_pclass_radius_flexslend(
            n_sides,
            r_circle,
            p_classification,
            lambda_flex,
            f_yield,
            fab_class)

    return return_formatter(sp)


def calc_from_area(
        n_sides,
        p_classification,
        area,
        length,
        f_yield,
        fab_class,
        **kargs
):
    sp = pg.TheoreticalSpecimen.from_pclass_area_length(
        n_sides,
        p_classification,
        area,
        length,
        f_yield,
        fab_class)

    return return_formatter(sp)


def calc_from_area_flexslend(
            n_sides,
            p_classification,
            area,
            lambda_flex,
            f_y,
            fab_class,
            **kargs
):
    sp = pg.TheoreticalSpecimen.from_pclass_area_flexslend(
            n_sides,
            p_classification,
            area,
            lambda_flex,
            f_y,
            fab_class
    )
    return return_formatter(sp)


def calc_from_area_radius(
            n_sides,
            r_cyl,
            area,
            length,
            f_y,
            fab_class,
            **kargs
):
    sp = pg.TheoreticalSpecimen.from_radius_area_length(
            n_sides,
            r_cyl,
            area,
            length,
            f_y,
            fab_class
    )
    
    return return_formatter(sp)

def main():
    pass
