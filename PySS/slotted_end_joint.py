import quantities as u
import PySS.steel_design as sd

# Define needed physical units
kN = u.UnitQuantity('kilonewton', u.newton*1000, symbol='kN')
mm = u.mm
MPa = u.MPa

gamma_M2 = 1.25

class bolt():
    def __init__(
            self,
            diam,
            grade,
            unthreaded=False,
            standard="4014"
    ):
        self.unthreaded = unthreaded
        std_diams = [16, 20] * mm
        std_grades = [8.8]
        if diam in std_diams:
            self.diam = int(diam)*mm
        if grade in std_grades:
            self.grade = grade
            (self.f_yb, self.f_ub) = sd.bolt_grade2stress(grade)*MPa
        self.area = bolt.calc_areas(self.diam)
        self.F_t_Rd = self.F_t_Rd()

    def F_t_Rd(self):
        if (self.grade in [4.6, 5.6, 8.8]) or self.unthreaded:
            a_v = 0.6
        else:
            a_v = 0.5

        return(a_v*self.f_ub*self.area/gamma_M2)

    @staticmethod
    def calc_areas(diam):
        try:
            diam.units = 'mm'
            return (u.pi*diam**2)/4
        except:
            print("Diameter must be given in length units")


class Connection():
    def __init__(
            self,
            N_Ed,
            n_bolts
    ):
        self.N_Ed = N_Ed
        self.n_bolts = n_bolts

    def __str__(self):
        return(
            "This is __str__: {} {}".format(
                self.N_Ed,
                self.n_bolts
            )
        )

    def __repr__(self):
        return(
            "This is __repr__: {}, {}".format(
                self.N_Ed,
                self.n_bolts
            )
        )

    def nu_bolt_shear(self):
        F_Ed = self.N_Ed / self.n_bolts
        F_Rd = 63*kN
        nu = F_Ed/F_Rd
        return(nu)


def __main__():
    case1 = Connection(100*kN, 10)
    print(case1.nu_bolt_shear())


if __name__ == "__main__":
    __main__()
