# -*- coding: utf-8 -*-

"""Top-level package for `PySS`."""

__author__ = """Panagiotis Manoleas"""
__email__ = 'f.sol@rbox.me'
__version__ = '0.7'
__release__ = '0.7.0'
__all__ = []

#from PySS import analytic_geometry, steel_design, fem, lab_tests, parametric
