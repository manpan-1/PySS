import pandas as pd
import PySS.steel_design as sd

pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)
desired_width = 320
pd.set_option('display.width', desired_width)

def load_tables(filepath=None):
    if filepath is None:
        filepath = "data/cs_prop_tables/"

    i_h = pd.read_csv(filepath+"I_H.csv", delimiter=";")
    shs = pd.read_csv(filepath+"SHS.csv", delimiter=';')

    del i_h["Drawing"]

    # Change units
    shs["A [mm2]"] = shs["A [cm2]"] * 100
    shs["I [mm4]"] = shs["I [cm4]"] * 10000
    shs["Wel [mm3]"] = shs["Wel [cm3]"] * 1000
    shs["Wpl [mm3]"] = shs["Wpl [cm3]"] * 1000

    return (i_h, shs)


def __main__():

    shs = load_tables(filepath="../data/cs_prop_tables/")[1]

    # print(shs)
    # ==Input==
    # length
    length = 3000

    # Yield stress
    f_y = 355

    # Point loads
    f_1 = 100e3
    f_2 = 6 * f_1

    # ==Resistance==
    # Elastic moment resistance
    shs["m_el"] = shs["Wel [mm3]"] * f_y

    # Plastic moment resistance
    shs["m_pl"] = shs["Wpl [mm3]"] * f_y

    # Tension (compression) resistance
    shs["n_t_rd"] = shs["A [mm2]"] * f_y

    # Flexural slenderness
    shs["lmbda"] = shs[["A [mm2]", "I [mm4]"]].apply(
        lambda x: sd.lmbda_flex(
            length,
            x[0],
            x[1],
            f_y,
            kapa_bc=0.7
        ),
        axis=1
    )

    # Reduction factor
    shs["chi"] = shs["lmdba"].apply(
        lambda x: sd.chi_flex(
            x,
            #length,
            #x[0],
            #x[1],
            #f_y,
            "c",
            kapa_bc=0.7
        ), axis=1
    )

    # Flexural buckling
    shs["n_b_rd"] = shs[["A [mm2]", "I [mm4]"]].apply(lambda x: sd.n_b_rd(length, x[0], x[1], f_y, "c", kapa_bc=0.7), axis=1)

    # ==Actions==
    # Eccentricity
    ecce = shs["h [mm]"] / 2

    # Acting moment
    shs["m_ed"] = ecce * f_2

    # Acting axial
    shs["n_ed"] = 2 * f_1 + f_2

    # ==Verification==
    # Check axial load
    # print("Compression verification")
    # print(n_t_rd > n_ed)

    # Check moment
    # print("Bending verification")
    # print(m_pl > m_ed)

    # Check interaction
    shs["a_w"] = (shs["A [mm2]"] - 2 * shs["b [mm]"] * shs["t [mm]"]) / (shs["A [mm2]"])
    shs["m_n_rd"] = shs["m_pl"] * (1 - (shs["n_ed"] / shs["n_t_rd"])) / (1 - 0.5 * shs["a_w"])
    # print("Interaction verification")
    # print(m_n_rd > m_ed)

    # Check flexural buckling
    # print("Buckling")
    # print(n_b_rd > n_ed)

    # Check buckling interaction
    c_m = 1
    k_1 = c_m * (1 + (shs["lmbda"] - 0.2) * shs["n_ed"] / (shs["chi"] * shs["n_t_rd"]))
    k_2 = c_m * (1 + 0.8 * shs["n_ed"] / (shs["chi"] * shs["n_t_rd"]))
    shs["k_yy"] = pd.concat([k_1, k_2], axis=1).min(axis=1)
    shs["n_int"] = shs["n_ed"] / (shs["chi"] * shs["n_t_rd"]) + shs["k_yy"] * shs["m_ed"] / shs["m_pl"]
    # print("Buckling interaction")
    # print(n_int <= 1)

    # Gather results
    shs["Compression"] = (shs["n_t_rd"] > shs["n_ed"])
    shs["Bending"] = (shs["m_pl"] > shs["m_ed"])
    shs["Interaction"] = (shs["m_n_rd"] > shs["m_ed"])
    shs["Buckling"] = (shs["n_b_rd"] > shs["n_ed"])
    shs["Buckling interaction"] = (shs["n_int"] <= 1)

    print(shs.loc[(shs["h [mm]"] == 150) & (shs["t [mm]"] == 8)].iloc[0])
    # print(shs[["h [mm]", "t [mm]", "Compression", "Bending", "Interaction", "Buckling", "Buckling interaction"]])


if __name__ == "__main__":
    __main__()
