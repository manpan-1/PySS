#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [
    'Click>=6.0', 'numpy', 'numpy-stl', 'matplotlib', 'scipy',
]

setup_requirements = [
    'Click>=6.0', 'numpy', 'numpy-stl', 'matplotlib', 'scipy',
]

test_requirements = [
    'Click>=6.0', 'numpy', 'numpy-stl', 'matplotlib', 'scipy',
]

setup(
    author="Panagiotis Manoleas",
    author_email='panagiotis.manoleas@paramatrixab.com',
    python_requires='>=3.5',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description="Python package for Steel Structures",
    install_requires=requirements,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='pyss',
    name='pyss',
    packages=find_packages(include=['pyss', 'pyss.*']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.com/manpan-1/pyss',
    version='0.6.0',
    zip_safe=False,
)

